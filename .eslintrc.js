module.exports = {
    'env': {
        'browser': true,
        'commonjs': true,
        'es2021': true
    },
    'overrides': [
    ],
    'parserOptions': {
        'ecmaVersion': 'latest'
    },
    'rules': {
        'linebreak-style': 'unix',
        'array-bracket-spacing': [2, 'never'],
        'block-scoped-var': 2,
        'brace-style': [2, 'allman', { 'allowSingleLine': true }],
        'camelcase': 0,
        'computed-property-spacing': [2, 'never'],
        'eol-last': 2,
        'new-cap': [1, { 'properties': false }],
        'no-console': 0,
        'no-extend-native': 2,
        'no-mixed-spaces-and-tabs': 2,
        'no-multi-spaces': 0,
        'no-trailing-spaces': 2,
        'no-unused-vars': 1,
        'no-undef': 1,
        'no-use-before-define': [2, 'nofunc'],
        'no-useless-escape': 1,
        'object-curly-spacing': [2, 'always'],
        'quotes': [2, 'single', 'avoid-escape'],
        'semi': ['error', 'always'],
        'keyword-spacing': [2, { 'before': true, 'after': true }],
        'space-before-function-paren': ['error', 'always'],
        'space-unary-ops': 2
    }
};
