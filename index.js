#!/usr/bin/env node
const path = require('node:path');
const FS = require('node:fs');
const process = require('node:process');
const EL = require('os').EOL;

const C = require('./src/_colors.js').Colors;
const { log } = require('./src/_utils.js');
const { create } = require('./src/Create.js');
const { serve } = require('./src/Serve.js');
const { develop } = require('./src/Develop.js');
const { build } = require('./src/Build.js');

// --------------------------------------------------------------------
// CLI arguments and their options
const COMMANDS =
[
  'help','-h','--help',
  'create','-c','--create',
  'serve','-s','--serve',
  'develop','-d','--develop',
  'build','-b','--build'
];
const OPTS =
{
  'create': ['templates','-t'],
  'serve': ['www','dev'],
  'build': ['www','dev','force','-f'],
};


// --------------------------------------------------------------------
// Functions

/**
 * Determines whether path is a valid SiteBuilder site
 * @param {*} path - to a directory
 * @returns true if path is to a valid site
 */
const getSiteIsValid = (site_path) =>
{
  let is_site_valid = true;
  if ( !FS.existsSync(site_path) )
  {
    log('project does not exist', 'getSiteIsValid', site_path, 'err');
    is_site_valid = false;
  }
  else if ( !FS.existsSync(path.join(site_path,'site_config.json')) )
  {
    log('project lacks site_config.json', 'getSiteIsValid', site_path, 'err');
    is_site_valid = false;
  }
  else if ( FS.existsSync(path.join(site_path,'src','content'))
      &&  FS.existsSync(path.join(site_path,'src','data'))
      &&  FS.existsSync(path.join(site_path,'src','styles'))
      &&  FS.existsSync(path.join(site_path,'src','templates'))
    )
  {
    is_site_valid = true;
  }
  else
  {
    log('project lacks requisite src', 'getSiteIsValid', site_path, 'err');
    is_site_valid = false;
  }

  if (!is_site_valid)
  {
    console.error(
      C.Reverse+C.Red+'  ERR  '+C.Reset+' '+C.Cyan+site_path+C.Reset + ' is not the root of a valid Site Builder project.'
    );
  }
  return is_site_valid;
};

/**
 * Display Help
 * triggered by the arguments -h | help
 */
const helpFeedback = () =>
{
  console.log(
    EL+C.Reverse+' SITE BUILDER '+C.Reset+' : '+C.Bright+'Help'+C.Reset+EL
   +EL
   +'This tool is a website generator.'+EL
   +''+EL
   +C.Bright+'USAGE:'+C.Reset+EL
   +C.Underscore+'ARG'+C.Reset+'                  '+C.Underscore+'MEANING'+C.Reset+EL
   +'[-c|'+C.Bright+'create'+C.Reset+']          Create a new Site Builder project for a website.'+EL
   +'    '+C.Dim+'<PATH>'+C.Reset+'             <PATH> to a directory for the new project. "newWebsite" will be created in the current directory if <PATH> is not provided.'+EL
   +'    '+C.Dim+'[-t|templates]'+C.Reset+'     Add starter documentation and templates to a project for you to learn from.'+EL
   +'[-b|'+C.Bright+'build'+C.Reset+']           Build the website.'+EL
   +'    '+C.Dim+'<PATH>'+C.Reset+'             <PATH> to the project to build. The current directory is default.'+EL
   +'    '+C.Dim+'[www|dev]'+C.Reset+'          www for a production build (default), dev for development.'+EL
   +'    '+C.Dim+'[-f|force]'+C.Reset+'         Force a rebuild of all content.'+EL
   +'[-d|'+C.Bright+'develop'+C.Reset+']         Execute watcher scripts to autobuild and serve the website during local development.'+EL
   +'    '+C.Dim+'<PATH>'+C.Reset+'             <PATH> to the project you are working on. The current directory is default.'+EL
   +'[-s|'+C.Bright+'serve'+C.Reset+']           Serve a Site Builder project locally.'+EL
   +'    '+C.Dim+'<PATH>'+C.Reset+'             <PATH> to the Site Builder directory to serve. The current directory is default.'+EL
   +'    '+C.Dim+'[www|dev]'+C.Reset+'          www for production, dev for development (default).'+EL
   +'[-h|'+C.Bright+'help'+C.Reset+']            Display this message.'+EL
   +''+EL
 );
};

const siteBuilder = async () =>
{
  const args = process.argv.slice(2);
  if (args.length)
  {
    log('', 'siteBuilder', args);

    // HELP
    if (args.includes('help')||args.includes('-h')||args.includes('--help'))
    {
      helpFeedback();
      process.exit(0);
    }

    // CREATE
    else if (args.includes('create')||args.includes('-c')||args.includes('--create'))
    {
      let index = args.indexOf('--create');
      if (index == -1)
      {
        index = args.indexOf('-c');
        if (index == -1)
          index = args.indexOf('create');
      }

      let this_path = 'newWebsite';
      let install_templates = false;
      args.splice(index, 1);
      args.forEach( (arg) =>
      {
        log(arg, 'CREATE', args, 'info', true);
        if (OPTS['create'].includes(arg) )
        {
          if (arg=='templates' || arg=='-t')
          {
            install_templates = true;
          }
        }
        else if (!COMMANDS.includes(arg))
        {
          this_path = arg;
        }
      });

      this_path = path.resolve(this_path);
      console.log(EL+C.Reverse+' SITE BUILDER '+C.Reset+' : '+C.Bright+'Create'+C.Reset+' '+C.Dim+this_path+C.Reset+EL);
      create(this_path, install_templates);
    }

    // SERVE
    else if (args.includes('serve')||args.includes('-s')||args.includes('--serve'))
    {
      let index = args.indexOf('--serve');
      if (index == -1)
      {
        index = args.indexOf('-s');
        if (index == -1)
          index = args.indexOf('serve');
      }

      let this_path = '';
      let server_directory = 'www_dev';
      args.splice(index, 1);
      args.forEach( (arg) =>
      {
        if (OPTS['serve'].includes(arg) )
        {
          if (arg=='www')
            server_directory = 'www';
        }
        else if (!COMMANDS.includes(arg))
        {
          this_path = arg;
        }
      });
      this_path = path.resolve(this_path);
      if (getSiteIsValid(this_path))
      {
        console.log(EL+C.Reverse+' SITE BUILDER '+C.Reset+' : '+C.Bright+'Serve'+C.Reset+' '+C.Dim+this_path+'/'+server_directory+C.Reset+EL);
        serve(this_path, server_directory, __dirname);
      }
    }

    // DEVELOP
    else if (args.includes('develop')||args.includes('-d')||args.includes('--develop'))
    {
      let index = args.indexOf('--develop');
      if (index == -1)
      {
        index = args.indexOf('-d');
        if (index == -1)
          index = args.indexOf('develop');
      }

      let this_path = '';
      // let server_directory = 'www_dev';
      args.splice(index, 1);
      args.forEach( (arg) =>
      {
        if (!COMMANDS.includes(arg))
        {
          this_path = arg;
        }
      });
      this_path = path.resolve(this_path);
      if (getSiteIsValid(this_path))
      {
        console.log(EL+C.Reverse+' SITE BUILDER '+C.Reset+' : '+C.Bright+'Develop'+C.Reset+' '+C.Dim+this_path+C.Reset+EL);
        develop(this_path, __dirname);
      }
    }

    // BUILD
    else if (args.includes('build')||args.includes('-b')||args.includes('--build'))
    {
      let index = args.indexOf('--build');
      if (index == -1)
      {
        index = args.indexOf('-b');
        if (index == -1)
          index = args.indexOf('build');
      }

      let this_path = '';
      let build_directory = 'www';
      let build_type = 'production';
      let force_content_processing = false;
      args.splice(index, 1);
      args.forEach( (arg) =>
      {
        if (OPTS['build'].includes(arg) )
        {
          if (arg=='dev')
          {
            build_directory = 'www_dev';
            build_type = 'development';
          }
          else if (arg=='force' || arg=='-f')
            force_content_processing = true;
        }
        else if (!COMMANDS.includes(arg))
        {
          this_path = arg;
        }
      });
      this_path = path.resolve(this_path);
      if (getSiteIsValid(this_path))
      {
        console.log(EL+C.Reverse+' SITE BUILDER '+C.Reset+' : '+C.Bright+'Build '+build_type+C.Reset+' '+C.Dim+this_path+'/'+build_directory+C.Reset+EL);
        build(this_path, build_directory, force_content_processing, __dirname);
      }
    }
  }
};


module.exports = siteBuilder();
