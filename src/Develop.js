const path = require('node:path');
// const FS = require('node:fs');
const process = require('node:process');
const spawn = require('node:child_process').spawn;
// const C = require('./_colors.js').Colors;
const { log } = require('./_utils.js');

/**
 * Setup watchers to autobuild and serve the Website located beneath <site_path>.
 * @param {string} site_path
 * @param {string} set_this_as_the_working_directory
 */
const develop = (site_path, set_this_as_the_working_directory) =>
{
  log(process.cwd(), 'develop', site_path+', '+set_this_as_the_working_directory, 'info');

  let developScriptPath = path.resolve(set_this_as_the_working_directory, 'scripts', 'develop.sh');
  let arguments = ['-p', site_path];
  spawn(developScriptPath, arguments, { cwd: set_this_as_the_working_directory, stdio: 'inherit' });
};

module.exports = { develop };
