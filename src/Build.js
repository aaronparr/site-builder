const path = require('node:path');
// const FS = require('node:fs');
const process = require('node:process');
const spawn = require('node:child_process').spawn;
// const C = require('./_colors.js').Colors;
const { log } = require('./_utils.js');

/**
 * Generate a website from the content at <site_path>.
 * @param {string} site_path
 * @param {string} build_directory - www or www_dev
 * @param {boolean} force - force rendering of all content
 * @param {string} set_this_as_the_working_directory
 */
const build = (site_path, build_directory, force, set_this_as_the_working_directory) =>
{
  log(process.cwd(), 'serve', site_path+', '+set_this_as_the_working_directory, 'info');

  let buildScriptPath = path.resolve(set_this_as_the_working_directory, 'scripts', 'build.sh');
  let arguments = ['-p', site_path, '-b', build_directory];
  if (force) { arguments.push('-f','true'); }
  spawn(buildScriptPath, arguments, { cwd: set_this_as_the_working_directory, stdio: 'inherit' });
};

module.exports = { build };
