const path = require('node:path');
const FS = require('node:fs');
const exec = require('node:child_process').exec;
const C = require('./_colors.js').Colors;
const { IS_LOGGING_ON } = require('./_globals.js');

/**
 * Wrapper for console.log for logging to console
 * @param {string} message
 * @param {string} funcName
 * @param {string} funcArgs
 * @param {string} logType
 * @param {boolean} logging - whether to log to console or not
 */
const log = (message, funcName, funcArgs = ' ', logType = 'info', logging=IS_LOGGING_ON) =>
{
  if (!logging){return;}

  let message_to_log = C.Cyan+funcName+C.Reset+'('+C.Pink+funcArgs+C.Reset+') ' + message;
  if (logType.includes('err')||logType.includes('warn'))
  {
    console.error(C.Red+C.Reverse+' '+logType.toUpperCase()+' '+C.Reset+' '+message_to_log);
  }
  else
  {
    console.log(C.Cyan+C.Reverse+' '+logType.toUpperCase()+' '+C.Reset+' '+message_to_log);
  }
};

/**
 * Takes the file extension and returns a file_type
 * @param {*} file_extension
 * @returns a string for the file_type. 'image' or 'text'
 */
const getFileType = (file_extension) =>
{
  let file_type = '';
  if ( file_extension.includes('txt') || file_extension.includes('md') )
  {
    file_type = 'text';
  }
  else if ( file_extension.includes('jpg') || file_extension.includes('gif') || file_extension.includes('png') )
  {
    file_type = 'image';
  }
  return file_type;
};

/**
 * returns an array containing all files and directories. Uses recursion to gather nested files and directories.
 * @param {*} directory - starting directory
 * @returns an array of paths each to a file or directory
 */
const getAllFiles = (directory) =>
{
  let allFiles = [];
  const filesInDirectory = FS.readdirSync(directory);
  for (const file of filesInDirectory)
  {
    const absolute = path.join(directory, file);
    if (FS.statSync(absolute).isDirectory())
    {
      allFiles.push(absolute);
      allFiles = allFiles.concat(getAllFiles(absolute));
    }
    else
    {
      allFiles.push(absolute);
    }
  }
  return allFiles;
};

/**
 * Execute command line
 * @param {string} command
 * @param {function} callback
 */
const execute = (command) =>
{
  log( '', 'execute', command);
  exec(command, (error, stdout, stderr) =>
  {
    if (error)
    {
      console.error(C.Red+C.Reverse+' ERR '+C.Reset+' '+error);
      return;
    }
    if (stdout) { console.log(stdout); }
    if (stderr) { console.error(stderr); }
  });
};

module.exports = { log, getFileType, getAllFiles, execute };
