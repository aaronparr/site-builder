const path = require('node:path');
const FS = require('node:fs');
const process = require('node:process');
const C = require('./_colors.js').Colors;
const EL = require('os').EOL;
const { log, getAllFiles } = require('./_utils.js');


/**
 * Create Site Directory and Initialize Files for Site Builder
 * @param {string} site_path
 */
const create = (site_path, install_templates) =>
{
  log(process.cwd(), 'createSite', site_path);

  let uninstalledResources = [];

  if (!FS.existsSync(site_path))
  {
    FS.mkdirSync( site_path, { recursive: true }, (err) => { if (err) throw err; } );
    console.log( site_path );
  }

  if (install_templates)
  {
    console.log(EL+C.Green+'Installing starter documentation and templates...'+C.Reset);
    let starterContent = getAllFiles(path.join(__dirname, '..', 'resources', 'starter'));
    // iterate over all starter resources
    starterContent.forEach( (orig_path) =>
    {
      let rel_path = orig_path.split(path.join('resources','starter'))[1];
      let copy_path= path.join(site_path, rel_path);
      if (!FS.existsSync(copy_path))
      {
        if ( FS.statSync(orig_path).isDirectory() )
        {
          FS.mkdirSync( copy_path, { recursive: false }, (err) => { if (err) throw err; } );
        }
        else
        {
          FS.copyFileSync(orig_path, copy_path);
        }
        console.log( C.Dim+path.dirname(copy_path)+' '+C.Reset+path.basename(copy_path));
      }
      else
      {
        if ( !FS.statSync(orig_path).isDirectory() )
        {
          uninstalledResources.push(copy_path);
        }
      }
    });
  }

  console.log(EL+C.Green+'Installing bare necessities...'+C.Reset);
  let bareContent = getAllFiles(path.join(__dirname, '..', 'resources', 'bare'));
  // iterate over all default resources
  bareContent.forEach( (orig_path) =>
  {
    let rel_path = orig_path.split(path.join('resources','bare'))[1];
    let copy_path= path.join(site_path, rel_path);
    if (!FS.existsSync(copy_path))
    {
      if ( FS.statSync(orig_path).isDirectory() )
      {
        FS.mkdirSync( copy_path, { recursive: false }, (err) => { if (err) throw err; } );
      }
      else
      {
        FS.copyFileSync(orig_path, copy_path);
      }
      console.log( C.Dim+path.dirname(copy_path)+' '+C.Reset+path.basename(copy_path));
    }
  });

  if ( uninstalledResources.length )
  {
    console.log(EL+C.Pink+'The following starter templates were not overwritten:'+C.Reset);
    uninstalledResources.forEach( (existingFile) =>
    {
      console.log(C.Dim+existingFile+C.Reset);
    });
  }
  console.log(EL+C.Green+'...done.'+C.Reset+EL);
};

module.exports = { create };
