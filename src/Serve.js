const path = require('node:path');
// const FS = require('node:fs');
const process = require('node:process');
const spawn = require('node:child_process').spawn;
// const C = require('./_colors.js').Colors;
const { log } = require('./_utils.js');

/**
 * Serve the Website located beneath <site_path>.
 * @param {string} site_path
 * @param {string} served_directory
 * @param {string} set_this_as_the_working_directory
 */
const serve = (site_path, served_directory, set_this_as_the_working_directory) =>
{
  log(process.cwd(), 'serve', site_path+', '+set_this_as_the_working_directory, 'info');

  let serveScriptPath = path.resolve(set_this_as_the_working_directory, 'scripts', 'serve.sh');
  let arguments = ['-p', site_path, '-s', served_directory];
  spawn(serveScriptPath, arguments, { cwd: set_this_as_the_working_directory, stdio: 'inherit' });
};

module.exports = { serve };
