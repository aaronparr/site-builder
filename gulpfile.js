const path = require('node:path');
const FS = require('node:fs');
const process = require('node:process');
const { Buffer } = require('node:buffer');
const http = require('node:http');
const https = require('node:https');
// const { PassThrough, Writable, Readable } = require('node:stream'); // through2 is currently handling this
// recommended transform wrapper for the readable stream
const through = require('through2');
// markdown to HTML converter
var Showdown  = require('showdown');
// hashing algorhythms
var CryptoJS = require('crypto-js');
// image resizing etc....
const sharp = require('sharp');
// Parse the metadata retrieved from a http request
const HTMLParser = require('node-html-parser');
//minify JS
const minify = require('gulp-minify');

// ---- GULP is THE BUSINESS right here ----
const { parallel, series, watch, src, dest } = require('gulp');
var Vinyl = require('vinyl'); // Gulp: data format of files in the stream
// example of a vinyl object's data structure
//     var jsFile = new Vinyl({
//       cwd: '/',
//       base: '/test/',
//       path: '/test/file.js',
//       contents: Buffer.from('var x = 123'),
//     });

// templating engine we are using...
const Handlebars = require('handlebars');

const { log, getFileType } = require('./src/_utils.js');

// --------------------------------------------------------------------
// Global Vars
let TRACKER = { lastUnlinkedPath: '', lastUnlinkedJsonPath: '' };
let PROJECT_PATH ='';
let DESTINATION_DIR_NAME = 'www_dev';
let FORCE_CONTENT_PROCESSING = false;
// check for build target
let args = process.argv.slice(2);
if (args.length)
{
  // process args
  args.forEach( (arg) =>
  {
    if (arg.includes('--forceContentProcessing'))
    {
      FORCE_CONTENT_PROCESSING = true;
    }
    else if (arg.includes('--buildTarget'))
    {
      DESTINATION_DIR_NAME = arg.split('=')[1];
    }
    else if (arg.includes('--projectPath'))
    {
      PROJECT_PATH = arg.split('=')[1];
    }
  });
}

let SITE  = JSON.parse(FS.readFileSync(path.join(PROJECT_PATH, 'site_config.json'), 'utf8'));
SITE.urlOrig = SITE.url;

const srcContentBasePath= path.join(PROJECT_PATH,'src', 'content');
const templatesBasePath  = path.join(PROJECT_PATH, 'src', 'templates');
const destinationPath   = path.join(PROJECT_PATH, DESTINATION_DIR_NAME);

// Globs
const optBaseSrc        = { cwd: PROJECT_PATH, base: srcContentBasePath };
const globSrcStatic     = ['./src/content/**', '!./src/content/**/*.hbs', '!./src/content/**/*.json', '!./src/content/**/*.md', '!./src/content/**/*.txt', './src/content/**/robots.txt', './src/content/**/@*/**', './src/content/.well-known/**', './src/content/inbox/**', './src/content/outbox/**', '!./src/content/**/*.js'];
const globSrcJS         = ['./src/content/**/*.js'];
const globSrcJson       = ['./src/content/**/*.json', '!./src/content/**/@*/**', '!./src/content/.well-known/**', '!./src/content/inbox/**', '!./src/content/outbox/**'];
const globSrcHandlebars = ['./src/content/**/*.hbs'];
const globSrcText       = ['./src/content/**/*.txt', './src/content/**/*.md', '!./src/content/**/robots.txt', '!./src/content/**/@*/**', '!./src/content/.well-known/**'];
const globSrcImage      = ['./src/content/**/*.jpg', './src/content/**/*.gif', './src/content/**/*.png', '!./src/content/**/@*/**', '!./src/content/.well-known/**', '!./src/content/favicon*'];
const globTemplates     = ['./src/templates/**'];
//
// --------------------------------------------------------------------

/**
 * Provide a URL to a web page
 * Function returns a promise which will resolve into an object containing the meta data from the head of the HTML
 * @param {*} pageUrl of link
 * @returns {*} {title: , meta: {description: , og:title, og:description, og:site_name, og:image, msapplication-TileImage, og:type, twitter:card, twitter:site, twitter:image}}
 */
async function updateLinkUrlData ( pageUrl, pageID='')
{
  if (pageID==='')
  {
    pageID = pageUrl;
    // look for a hash and strip it away
    if (pageID.includes('#'))
    {
      pageID = pageID.split('#')[0];
    }
    // we should probably leave the parameter as that can lead to a different page
  }

  //
  let thisPageUrl = new URL(pageUrl);
  let thisRequestMethod = https;
  let thisPort = 443;
  if ( !pageUrl.includes('https') )
  {
    thisRequestMethod = http;
    thisPort = 80;
  }

  const options =
  {
    hostname: thisPageUrl.hostname,
    port: thisPort,
    path: escape(thisPageUrl.pathname)+thisPageUrl.search,
    method: 'GET',
    headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15' }
  };

  var pageHtml = '';
  const req = thisRequestMethod.request(options, (res) =>
  {
    let response_code = res.statusCode;

    // all non-successful responses are skipped
    // failed requests keep hanging so....
    if (response_code<200 || response_code>399)
    {
      log('Aborting with status '+response_code, 'updateLinkUrlData', thisPageUrl, 'warn', true );
      req.abort();
      return;
    }
    else if (response_code>299 && response_code<399)
    {
      req.abort();
      if (res.headers.location!==pageUrl)
      {
        log('Redirect '+res.headers.location, 'updateLinkUrlData', thisPageUrl, 'warn', true );
        updateLinkUrlData ( res.headers.location, pageID);
      }
      else
      {
        log('Aborting with status '+response_code, 'updateLinkUrlData', thisPageUrl, 'warn', true );
      }
      return;
    }

    res.on('data', (d) =>
    {
      pageHtml += d;
    });
    res.on('end' , () =>
    {
      let head =
      {
        'title' : '',
        'meta' :
        {
          'description' : '',
          'og:title' : '',
          'og:description' : '',
          'og:site_name' : '',
          'og:image' : '',
          'msapplication-TileImage' : '',
          'og:type' : '',
          'twitter:card' : '',
          'twitter:site' : '',
          'twitter:image' : '',
          'twitter:player' : ''
        }
      };

      let page = HTMLParser.parse(pageHtml);
      // head.title
      let elem_title = page.querySelector('title');
      head.title = elem_title ? elem_title.text : '';
      // this should not happen
      if (head.title.includes('Bad Request') || head.title.includes('Server Error'))
      {
        log(' '+head.title +' with status '+response_code, 'updateLinkUrlData', thisPageUrl, 'warn', true );
        return;
      }

      // head.meta.description
      let elem_meta_description = page.querySelector("meta[name='description']");
      head.meta['description'] = elem_meta_description ? elem_meta_description._rawAttrs.content : '';
      // head.meta.msapplication-TileImage
      let elem_meta_ms_tileimage = page.querySelector("meta[property='msapplication-TileImage']");
      if (!elem_meta_ms_tileimage)
          elem_meta_ms_tileimage = page.querySelector("meta[name='msapplication-TileImage']");
      head.meta['msapplication-TileImage'] = elem_meta_ms_tileimage ?  elem_meta_ms_tileimage._rawAttrs.content : '';
      // head.meta.twitter:card
      let elem_meta_twitter_card = page.querySelector("meta[property='twitter:card']");
      if (!elem_meta_twitter_card)
          elem_meta_twitter_card = page.querySelector("meta[name='twitter:card']");
      head.meta['twitter:card'] = elem_meta_twitter_card ? elem_meta_twitter_card._rawAttrs.content : '';
      // head.meta.twitter:site
      let elem_meta_twitter_site = page.querySelector("meta[property='twitter:site']");
      if (!elem_meta_twitter_site)
          elem_meta_twitter_site = page.querySelector("meta[name='twitter:site']");
      head.meta['twitter:site'] = elem_meta_twitter_site ? elem_meta_twitter_site._rawAttrs.content : '';
      // head.meta.twitter:image
      let elem_meta_twitter_image = page.querySelector("meta[property='twitter:image']");
      if (!elem_meta_twitter_image)
          elem_meta_twitter_image = page.querySelector("meta[name='twitter:image']");
      head.meta['twitter:image'] = elem_meta_twitter_image ? elem_meta_twitter_image._rawAttrs.content : '';
      // head.meta.twitter:player
      let elem_meta_twitter_player = page.querySelector("meta[property='twitter:player']");
      if (!elem_meta_twitter_player)
          elem_meta_twitter_player = page.querySelector("meta[name='twitter:player']");
      head.meta['twitter:player'] = elem_meta_twitter_player ? decodeURIComponent(elem_meta_twitter_player._rawAttrs.content) : '';
      // head.meta.og:title
      let elem_meta_og_title = page.querySelector("meta[property='og:title']");
      head.meta['og:title'] =  elem_meta_og_title ? elem_meta_og_title._rawAttrs.content : '';
      // head.meta.og:description
      let elem_meta_og_description = page.querySelector("meta[property='og:description']");
      head.meta['og:description'] = elem_meta_og_description ? elem_meta_og_description._rawAttrs.content : '';
      // head.meta.og:image
      let elem_meta_og_image = page.querySelector("meta[property='og:image']");
      head.meta['og:image'] = elem_meta_og_image ? elem_meta_og_image._rawAttrs.content : '';
      // head.meta.og:site-name
      let elem_meta_og_sitename = page.querySelector("meta[property='og:site_name']");
      head.meta['og:site_name'] = elem_meta_og_sitename ? elem_meta_og_sitename._rawAttrs.content : '';
      // head.meta.og:type
      let elem_meta_og_type = page.querySelector("meta[property='og:type']");
      head.meta['og:type'] = elem_meta_og_type ? elem_meta_og_type._rawAttrs.content : '';
      // head.meta.og:type
      let elem_meta_og_video_url = page.querySelector("meta[property='og:video:url']");
      head.meta['og:video:url'] = elem_meta_og_video_url ? decodeURIComponent(elem_meta_og_video_url._rawAttrs.content) : '';

      // AND NOW WE UPDATE links.json
      let thisLinkJson = JSON.parse( FS.readFileSync(path.join(PROJECT_PATH, 'src','data','links.json')) );
      let thisLinkIndex = thisLinkJson.links.findIndex( linkRecord => linkRecord.url === pageID );

      let update = false;
      let linkRecord = { };
      let historyLength = 0;
      if (thisLinkIndex>-1)
      {
        historyLength = thisLinkJson.links[thisLinkIndex].history.length;
        if (thisLinkJson.links[thisLinkIndex].update)
        {
          update = true;
        }
      }

      if (update || thisLinkIndex==-1)
      {
        // PREVIEW IMAGE PRE-PROCESSING -----------
        let thisImageSplash = true;
        let theProspectiveImage = head['meta']['twitter:image'];
        // Look for a logo associated with this domain
        // FIRST get domain
        let thisDomain = new URL(pageID).host;
        // SECOND look for a logo associated with a domain in the "logos" list
        if (thisDomain.includes('www.'))
        {
          thisDomain = thisDomain.substring(thisDomain.indexOf('www.')+4);
          thisLinkJson.logos.forEach( (link) =>
          {
            if ( thisDomain === link.domain )
            {
              theProspectiveImage = link.logo;
              thisImageSplash = false; // logos are not splashy. we want a small preview
              return;
            }
          });
        }
        else
        {
          thisLinkJson.logos.forEach( (link) =>
          {
            if ( thisDomain.includes(link.domain) )
            {
              theProspectiveImage = link.logo;
              thisImageSplash = false; // logos are not splashy. we want a small preview
              return;
            }
          });
        }

        // -------------------------------------------

        linkRecord = {
          url: pageID,
          local: (new URL(pageID).hostname.includes(new URL(SITE.urlOrig).hostname)),
          site: head['meta']['og:site_name'],
          title: head['meta']['og:title'],
          description: head['meta']['og:description'],
          image: theProspectiveImage,
          player: head['meta']['twitter:player'],
          imageSplash: thisImageSplash,
          update: update,
          index: historyLength,
          images: [],
          history: []
        };
        if (!head.meta['og:title']) { linkRecord.title = head.title;}
        if (!head.meta['og:description']) { linkRecord.description = head.meta['description'];}
        if (!head.meta['og:site_name'])
        {
          linkRecord.site = new URL(pageID).hostname;
        }
        if (!theProspectiveImage)
        {
          if (head.meta['msapplication-TileImage'])
          {
            linkRecord.image = head.meta['msapplication-TileImage'];
            linkRecord.imageSplash = false;
          }
          else if (head.meta['og:image'] )
          {
            linkRecord.image = head.meta['og:image'];
          }
          else
          {
            linkRecord.imageSplash = false;
          }
        }
        if (linkRecord.image)
        {
          // if thre are parameters delivered to this image... delete them
          if (linkRecord.image.includes('?')) { linkRecord.image = linkRecord.image.split('?')[0]; }
        }
        if (!head.meta['twitter:player'])
        {
          if (head.meta['go:video:url'])
          {
            linkRecord.player = head.meta['go:video:url'];
          }
        }
      }
      else
      {
        linkRecord = {
          url: pageID,
          local: thisLinkJson.links[thisLinkIndex].local,
          site: thisLinkJson.links[thisLinkIndex].site,
          title: thisLinkJson.links[thisLinkIndex].title,
          description: thisLinkJson.links[thisLinkIndex].description,
          image: thisLinkJson.links[thisLinkIndex].image,
          player: thisLinkJson.links[thisLinkIndex].player,
          imageSplash: thisLinkJson.links[thisLinkIndex].imageSplash,
          update: false,
          index: thisLinkJson.links[thisLinkIndex].index,
          images: thisLinkJson.links[thisLinkIndex].images,
          history: []
        };
      }

      // images -------
      let image_src_list = [];
      try { if (linkRecord.images.length) { image_src_list = linkRecord.images; } }
      catch { linkRecord.images = []; }
      // get the src of each img on the page
      let all_elem_images = page.querySelectorAll('img');
      all_elem_images.forEach(
        (image) =>
        {
          let imgSrc = image.getAttribute('src');
          // add a src which we have not yet captured
          if (!image_src_list.includes(imgSrc) && imgSrc!=undefined) { image_src_list.push(imgSrc); }
        }
      );
      // update the linkRecord?
      let skipImageUpdate = false;
      if (image_src_list.length != linkRecord.images.length) { linkRecord.images = image_src_list; }
      else { skipImageUpdate = true; }

      // history -------
      if (thisLinkIndex>-1)
      {
        linkRecord.history = thisLinkJson.links[thisLinkIndex].history;
      }
      head['index'] = historyLength-1;
      if (JSON.stringify(head) !== JSON.stringify(linkRecord.history[linkRecord.history.length-1]))
      {
        head['index'] = historyLength;
        linkRecord.history.push(head);
      }
      else if (skipImageUpdate && !FORCE_CONTENT_PROCESSING)
      {
        return;
      }

      // update the file
      if (thisLinkIndex==-1)
      {
        thisLinkJson.links.push( linkRecord );
      }
      else
      {
        thisLinkJson.links[thisLinkIndex] = linkRecord;
      }

      FS.writeFileSync(path.join(PROJECT_PATH, 'src', 'data', 'links.json'), JSON.stringify(thisLinkJson, null, 2));
    });
  });

  req.on('error', (e) =>
  {
    // console.error(e);
    return;
  });
  req.end();
}

function getCopyrightYear (dateTimeString)
{
  return new Date(dateTimeString)
  .toLocaleDateString('en-us', { timeZone: 'America/Los_Angeles', year:'numeric' } )
  ;
}

function getDateString (dateTimeString)
{
  return new Date(dateTimeString)
    .toLocaleDateString('en-us', { timeZone: 'America/Los_Angeles', year:'numeric', month:'short', day:'numeric' } )
    + '<span class="disappear-small">'
    + getTimeOfDayString(new Date(dateTimeString)
    .toLocaleDateString( 'en-us', { timeZone: 'America/Los_Angeles', hour:'numeric' } ) )
    + '</span>'
    ;
}

function getTimeOfDayString ( localeDate )
{
  let timeOfDayString = ', ';
  let time = localeDate.split(',')[1];
  let hour = parseInt(time);
  if (time.includes('P') && hour!=12)
    hour += 12;

  if (hour == 0 || hour == 24) { timeOfDayString += 'midnight'; }
  else if (hour==1) { timeOfDayString += 'after midnight'; }
  else if (hour==2) { timeOfDayString += 'in the small hours'; }
  else if (hour==3) { timeOfDayString += 'the devil\'s hour'; }
  else if (hour==4) { timeOfDayString += 'in the small hours'; }
  else if (hour==5) { timeOfDayString += 'pre-dawn'; }
  else if (hour==6) { timeOfDayString += 'dawn'; }
  else if (hour>=7 && hour<=10) { timeOfDayString += 'morning'; }
  else if (hour==11) { timeOfDayString += 'almost noon'; }
  else if (hour==12) { timeOfDayString += 'noon'; }
  else if (hour==13) { timeOfDayString += 'noonish'; }
  else if (hour==14) { timeOfDayString += 'afternoon'; }
  else if (hour==15) { timeOfDayString += 'teatime'; }
  else if (hour==16) { timeOfDayString += 'afternoon'; }
  else if (hour==17) { timeOfDayString += 'afternoon'; }
  else if (hour==18) { timeOfDayString += 'dusk'; }
  else if (hour==19) { timeOfDayString += 'evening'; }
  else if (hour>=20 && hour<23) { timeOfDayString += 'night'; }
  else if (hour>=23 ) { timeOfDayString += 'at the eleventh hour'; }
  else { timeOfDayString += 'in my time of dying'; }


  return timeOfDayString;
}

/**
 * Provide a file path of a piece of content. Receive a populated obejct on success. Empty on failure.
 * Function seeks an index.json to gather properties `config.type` and `config.template`
 * A `child` type indicates that the parent directory contains similar content.
 * The path returned is of an index.json which holds this content's list
 * @param {*} filepath of content file
 * @returns {*} {'type': defines content, 'content_type': sort type for content, 'path': to parent index.json holding the `list`, 'content_template': template for content pages, 'publish_list: an array of paths to json to post content to, 'title': for head, 'description': for head}
 */
function getConfigDataForFile (file)
{
  let listConfig  =
  {
    type: null,
    flatten_child_directories: null,
    content_type: null,
    content_category: null,
    path: null,
    content_template: null,
    publish_list: [],
    title: null,
    description: null,
    author: null
  };

  // traverse the tree looking for a value for [index.json].config.type
  // if the type is 'child' look in the parent directory
  // if the type is valid then we found our file
  let tempDir = path.dirname(file.path);
  let tempConfigPath = path.join(tempDir, 'index.json');
  // log(tempConfigPath, 'getConfigDataForFile', '', 'info', true);
  while (FS.existsSync(tempConfigPath))
  {
    const JSONString = FS.readFileSync(tempConfigPath, 'utf8');
    const tempConfigJson = JSON.parse(JSONString);

    if ( tempConfigJson.config && tempConfigJson.config.type )
    {
      // we grab the first template reference we find in the tree
      // and store it on the returned object as content_template
      if ( listConfig.content_template===null)
      {
        if ( tempConfigJson.config.template_content )
        {
          listConfig.content_template = tempConfigJson.config.template_content;
        }
        else if ( tempConfigJson.config.template )
        {
          listConfig.content_template = tempConfigJson.config.template;
        }
      }
      if ( listConfig.flatten_child_directories===null )
      {
        if ( tempConfigJson.config.flatten_child_directories===true || tempConfigJson.config.flatten_child_directories===false)
        {
          listConfig.flatten_child_directories = tempConfigJson.config.flatten_child_directories;
        }
      }
      // we add to the publish_list each time we find new publish targets
      if ( tempConfigJson.config.list_publish )
      {
        tempConfigJson.config.list_publish.forEach( (list) =>
        {
          if (!listConfig.publish_list.includes(list))
            listConfig.publish_list.push(list);
        });
      }
      // we take the first content_type we find
      if ( listConfig.content_type===null && tempConfigJson.config.content_type)
      {
        listConfig.content_type = tempConfigJson.config.content_type;
      }
      // we take the first content_category we find
      if ( listConfig.content_category===null && tempConfigJson.config.content_category)
      {
        listConfig.content_category = tempConfigJson.config.content_category;
      }
      // we take the first author we find
      if ( listConfig.author===null && tempConfigJson.props.author)
      {
        listConfig.author = tempConfigJson.props.author;
      }
      // we take the first head_title we find
      if ( listConfig.title===null && tempConfigJson.props.head_title)
      {
        listConfig.title = tempConfigJson.props.head_title;
      }
      // we take the first head_description we find
      if ( listConfig.description===null && tempConfigJson.props.head_description)
      {
        listConfig.description = tempConfigJson.props.head_description;
      }

      // ? is this index just a child of a parent dir
      if ( tempConfigJson.config.type == 'child' )
      {
        // climb up to the parent directory
        tempDir = path.join(tempDir, '..');
        if (tempDir=='src')
        {
          return {}; // we've reached beneath our root -- exit function with nothing
        }
        else
        {
          tempConfigPath = path.join(tempDir, 'index.json'); // we'll continue in the parent dir
        }
      }
      else
      {
        // we found it - so lets store the values we need
        // type of list
        listConfig.type = tempConfigJson.config.type;
        // path to the list config should we want to write to the json
        listConfig.path = path.relative(PROJECT_PATH, tempConfigPath);
        break;
      }
    }
    else
    {
      return {}; // we've reached a dead node -- exit function with nothing
    }
  }

  return listConfig;
}

function getContentJsonForFile (file, extension, watchedChange=false)
{
  // Look for a json file full of data to be used in the page
  let contentJson = { 'config': {}, 'props': {} };
  let listConfig  = getConfigDataForFile(file);

  // - get the path of the plaintext file's paired json file
  const dataFile = file.path.replace(extension, '.json');
  // ? does the paired json file exist?
  if (FS.existsSync(dataFile))
  {
    // log(dataFile, 'getContentJsonForFile', file.path, 'info', true);
    // - it exists, let's grab that data and override
    contentJson = JSON.parse(FS.readFileSync(dataFile, 'utf8'));
    // some files were missing this so it is added here now too
    if (!contentJson.config.contentPath) { contentJson.config.contentPath = path.relative(PROJECT_PATH, file.path); }
    // if no change is detected between the file and its data file then we should skip updating the json
    const file_type = getFileType(extension);

    if ( file_type === 'text' )
    {
      let file_contents_salted = file.contents + file.contents.length.toString();
      let latest_content_hash = CryptoJS.MD5(file_contents_salted).toString();
      if (    contentJson.config.content_hash
          &&  latest_content_hash
          && (contentJson.config.content_hash === latest_content_hash)
        )
      {
        contentJson.config.content_skip_processing = true;
        if (!FORCE_CONTENT_PROCESSING)
          return contentJson;
      }
      else
      {
        contentJson.config.content_hash = latest_content_hash;
        contentJson.config.content_skip_processing = false;
      }
    }
    else if ( file_type === 'image' )
    {
      const stats = FS.statSync(file.path);
      let latest_date_modified = stats.mtime.toString();
      if (    contentJson.config.content_hash
          &&  latest_date_modified
          && (contentJson.config.content_hash === latest_date_modified)
        )
      {
        contentJson.config.content_skip_processing = true;
        if (!FORCE_CONTENT_PROCESSING)
          return contentJson;
      }
      else
      {
        contentJson.config.content_hash = latest_date_modified;
        contentJson.config.content_skip_processing = false;
      }
    }
  }
  else
  {
    // paired json does not exist (it may need to be created for the user)
    // we thus need to initialize the data for later:
    //       the location of the content that the .json stores metadata for
    contentJson.config.contentPath = path.relative(PROJECT_PATH,file.path);
  }

  contentJson.config.self = path.relative(PROJECT_PATH, dataFile);;

  // - data missing in the content's json?
  if (!contentJson.config.template || !contentJson.config.type)
  {
    // ? does an appropriate index.json exist?
    if (listConfig.path)
    {
      if (listConfig.type && listConfig.type!=='list')
      {
        contentJson.config.list_path = listConfig.path;
        // use this index.json set what we need to our content's data
        if (!contentJson.config.type)
          contentJson.config.type = listConfig.type;
        if (!contentJson.config.template && listConfig.content_template)
          contentJson.config.template = listConfig.content_template;
      }
    }
  }
  else if (listConfig.path && contentJson.config.type===listConfig.type)
  {
    contentJson.config.list_path = listConfig.path;
  }


  // FILE DATES
  // created    contentJson.config.date_created
  // modified   contentJson.config.date_modified
  // published  contentJson.config.date_published
  //
  // - initialize date if it is not yet set
  let date_now = new Date();
  if ( !contentJson.config.date_created )
  {
    contentJson.config.date_created = date_now.toJSON();
  }
  // restrict update of date modified
  // because this executes on start up (initialization) even when nothing has changed
  if (watchedChange || !contentJson.config.date_modified)
  {
    contentJson.config.date_modified = date_now.toJSON();
  }

  // props.date is special as a human readable publish date on the file
  // actual dates are stored in config
  if ( contentJson.config.date_published )
  {
    // we should set the date property here
    if ( !contentJson.props.copyright_year )
      contentJson.props.copyright_year = getCopyrightYear(contentJson.config.date_published);
    contentJson.props.date = getDateString(contentJson.config.date_published);
  }
  else
  {
    // this content isn't published yet but let us provide a date to view in dev
    contentJson.props.date =
      date_now.toLocaleDateString( 'en-us', { timeZone: 'America/Los_Angeles', year:'numeric', month:'short', day:'numeric' } )
      + '<span class="disappear-small">'
      + getTimeOfDayString( date_now.toLocaleDateString( 'en-us', { timeZone: 'America/Los_Angeles', hour:'numeric' } ) )
      + '</span>'
      ;
  }

  // ? is this actual content that we will compile into a page?
  if (contentJson.config.template && contentJson.config.type)
  {
    if (listConfig.publish_list && listConfig.publish_list.length>0)
    {
      if (contentJson.config.list_publish==undefined || contentJson.config.list_publish==null)
      {
        contentJson.config.list_publish = listConfig.publish_list;
      }
      else if (contentJson.config.list_publish.length==0)
      {
        // leave it be
        // this gives us the option to not publish a particular piece of content outside of its directory
      }
      else
      {
        listConfig.publish_list.forEach( (list) =>
        {
          if (!contentJson.config.list_publish.includes(list))
            contentJson.config.list_publish.push(list);
        });
      }
    }
    if (!contentJson.props.head_title && listConfig.title)
    {
      contentJson.props.head_title = listConfig.title;
    }
    if (!contentJson.props.head_description && listConfig.description)
    {
      contentJson.props.head_description = listConfig.description;
    }
    if (!contentJson.props.author)
    {
      if(listConfig.author)
        contentJson.props.author = listConfig.author;
      else
        contentJson.props.author = SITE.author;
    }
    if (!contentJson.config.content_type && listConfig.content_type)
    {
      contentJson.config.content_type = listConfig.content_type;
    }
    if (!contentJson.props.content_category && listConfig.content_category)
    {
      contentJson.props.content_category  = listConfig.content_category;
    }
    // since this is compiled content we need an ID
    // permanent ID used in URLs to the content - initialize if not setup
    if (!contentJson.props.id)
    {
      const timestamp = contentJson.config.date_created.replaceAll(':','').replaceAll('.','').replaceAll('-','').replaceAll('T','_');
      // if (contentJson.config.type==='status')
      // {
        // ID is the date of creation
        contentJson.props.id = timestamp;
      // }
      // // ...if nothing else...
      // else
      // {
      //   // use the file path as its unique identifier but hash it to obscure it
      //   contentJson.props.id = CryptoJS.MD5(CryptoJS.SHA256(path.relative(PROJECT_PATH,file.path)+SITE.salt).toString()).toString();
      // }
    }

    // FILE relative URL and Links
    //
    let relPath = path.relative(srcContentBasePath, file.path);
    let relDirPath = path.dirname(relPath);
    let localUrlBase = '/' + relDirPath.replaceAll('\\','/');
    if (listConfig.flatten_child_directories)
    {
      contentJson.config.flatten_child_directories = listConfig.flatten_child_directories;
      let relListPath = path.relative(srcContentBasePath, path.join(PROJECT_PATH, contentJson.config.list_path));
      let relListDirPath = path.dirname(relListPath);
      localUrlBase = '/' + relListDirPath.replaceAll('\\','/');
    }
    // for files in the root we need to make this exception
    if (localUrlBase=='/.') { localUrlBase = ''; }

    if (contentJson.config.type==='status')
    {
      let baseName = contentJson.props.id;
      // an odd exception
      if (baseName==undefined || !baseName) { baseName = ''; }

      // if (!contentJson.config.link_self)
        contentJson.config.link_self = localUrlBase+'/'+ baseName;

      if (contentJson.config.parent_id)
      {
        // if (!contentJson.config.link_parent)
          contentJson.config.link_parent = localUrlBase+'/'+ contentJson.config.parent_id + '#' +baseName;
        // unless this turns out to be a update about something
        contentJson.props.link_thing = contentJson.config.link_parent;
      }
      else
      {
        // if (!contentJson.config.link_parent)
          contentJson.config.link_parent = localUrlBase+ '#' +baseName;
        // this might not be set easly enough to work here
        if (contentJson.props.list_content)
        {
          // unless this turns out to be a update about something
          contentJson.props.link_thing = contentJson.config.link_self;
        }
        else
        {
          contentJson.props.link_thing = contentJson.config.link_parent;
        }
      }
    }
    else
    {
      let baseName = path.basename(file.path, path.extname(file.path));
      if (baseName == 'index'){ baseName = ''; }
      // if (!contentJson.config.link_self )
        contentJson.config.link_self = localUrlBase+'/'+ baseName;
      // if (!contentJson.config.link_parent)
        contentJson.config.link_parent = localUrlBase+ '#' +contentJson.props.id;
      if (contentJson.config.type==='fragment' || contentJson.config.type=='pangram')
      {
        contentJson.props.link_thing = contentJson.config.link_parent;
      }
      else
      {
        contentJson.props.link_thing = contentJson.config.link_self;
      }
    }
    if (contentJson.config.link_thing)
      contentJson.props.link_thing = contentJson.config.link_thing;
    contentJson.props.link_self = contentJson.config.link_self;
    contentJson.props.link_parent = contentJson.config.link_parent;
    contentJson.props.link_redirect = contentJson.props.link_thing;
  }

  return contentJson;
}

function writeContentJsonFile (file, fileJson)
{
  // console.log('writeContentJsonFile()');
  if ((DESTINATION_DIR_NAME !== 'www_dev' && !fileJson.config.date_published) && fileJson.props.date)
  {
    delete fileJson.props.date;
  }

  // --

  FS.writeFileSync(file.path.replace(path.extname(file.path),'.json'), JSON.stringify(fileJson, null, 2));
}

function getTemplateForContent (contentJson)
{
  let template = null;
  let templateName = contentJson.config.template;
  if (!templateName)
    return null;
  if (templateName.lastIndexOf('.')==-1) { templateName += '.hbs'; }
  let templateFilePath = '';
  if (templateName.indexOf('\\') != -1 || templateName.indexOf('/') != -1)
  {
    templateFilePath = path.join(PROJECT_PATH, templateName);
  }
  else
  {
    templateFilePath = path.join(templatesBasePath, templateName);
  }

  if (FS.existsSync(templateFilePath))
  {
    template = FS.readFileSync(templateFilePath, 'utf8');
  }

  return template;
}

// --------------------------------------------------------------------
// TEMPLATING
// we use handlebars templates to structure our views
// they allow us to break a page into reusuable chunks (HEADER, FOOTER etc...)
// and to separate content/data from the page itself.

/**
 * Registers for Handlebars all templates in _partials
 */
function handlebarsRegisterAllPartials ()
{
  // console.log('handlebarsRegisterAllPartials');
  const partialsDir = path.join(templatesBasePath, '_partials');
  const fileNames = FS.readdirSync(partialsDir);
  // - iterate over all partial templates in /templates/_partials
  fileNames.forEach(function (fileName)
  {
    // partial identifier - need to exclude the extension from the file name
    const matches = /^([^.]+).hbs$/.exec(fileName);
    if (!matches) { return; }
    const partialName = matches[1];
    // template - we convert the file to a string
    const template = FS.readFileSync(path.join(partialsDir, fileName), 'utf8');
    // --> register the partial with handlebars <--
    // console.log('registering '+partialName);
    Handlebars.registerPartial(partialName, template);
  });

  // lets also expand handlebars with some helpers
  Handlebars.registerHelper('compare', function ()
  {
    let evaluatedExpression = '';
    for (let i = 0; i < arguments.length-1; i++)
    {
      if (    arguments[i] === '!'
          ||  arguments[i] === '!='
          ||  arguments[i] === '!=='
          ||  arguments[i] === '='
          ||  arguments[i] === '=='
          ||  arguments[i] === '==='
          ||  arguments[i] === '<'
          ||  arguments[i] === '<='
          ||  arguments[i] === '>='
          ||  arguments[i] === '>'
          ||  arguments[i] === '&&'
          ||  arguments[i] === '||'
          ||  arguments[i] === 'null'
          ||  arguments[i] === '('
          ||  arguments[i] === ')'
          ||  arguments[i] === '+'
          ||  arguments[i] === '-'
          ||  arguments[i] === '%'
          ||  typeof arguments[i] === 'number'
         )
      {
        evaluatedExpression += ' ' + arguments[i];
      }
      else if (typeof arguments[i] === 'string')
      {
        evaluatedExpression += ' "' + arguments[i] + '"';
      }
      else
      {
        evaluatedExpression += ' ' + arguments[i];
      }
    }
    // log('', 'handlebarsRegisterAllPartials', evaluatedExpression, 'info' );
    if (eval(evaluatedExpression))
    {
      return true;
    }
    else
    {
      return false;
    }
  });

  Handlebars.registerHelper('isLinkDeeper', function (link, base)
  {
    link = String(link);
    if (link.includes(base) && link != base )
    {
      return true;
    }
    else
    {
      return false;
    }
  });

  // this will only work on subterfugue
  Handlebars.registerHelper('galleryLink', function (imageLinkParent)
  {
    let galleryLink = String(imageLinkParent);
    // now we need to transform this
    if (galleryLink.includes('/img'))
    {
      galleryLink = galleryLink.replace('/img','/gallery');
      if (galleryLink.includes('#'))
        galleryLink = galleryLink.substring(0, galleryLink.indexOf('#'));
    }
    return galleryLink;
  });

  // ok... let 'em know we're done.
  // return Promise.resolve();
}



/**
 * Deletes lists on json files
 * Lists of content and hashes lack garbage collection
 * so we need to delete them on initialization at startup and before builds
 * The lists are generated fresh when content is compiled
 * @param {*} callback -- default value
 * @returns the file stream created by through2
 */
function deleteListsFromJson ()
{
  // Create a stream through which each .md will pass
  var stream = through.obj(function (file=new Vinyl(), enc, callBack)
  {
    // parse this into JSON
    // console.log(file.contents.toString());
    log('', 'deleteListsFromJson', file.path, 'info');
    let contentJson = JSON.parse(file.contents.toString());
    if ( contentJson.props && contentJson.props.list_content )
    {
      delete contentJson.props.list_content;
      FS.writeFileSync(file.path, JSON.stringify(contentJson,null,2));
    }

    // tell the stream engine that we are done with this file
    callBack();
  });

  // returning the file stream
  return stream;
}

function sortContentList (contentList, sortMethod = '')
{
  // console.log('sortContentList ');
  if (!sortMethod || sortMethod=='' || sortMethod=='descending')
  {
    contentList.sort((a, b) =>
    {
      if (a.date < b.date)
      {
        return 1; // sort a after b
      }
      else if (a.date > b.date)
      {
        return -1; // sort a before b
      }
      else
      {
        return 0; // keep original order
      }
    });
  }
  else if (sortMethod=='chronological' || sortMethod=='ascending')
  {
    contentList.sort((a, b) =>
    {
      if (a.date > b.date)
      {
        return 1; // sort a before b
      }
      else if (a.date < b.date)
      {
        return -1; // sort a after b
      }
      else
      {
        return 0; // keep original order
      }
    });
  }

  return contentList;
}

function convertHashesToTags ( contentText, contentJson)
{
  let text = contentText;

  let hashesFound = text.match(/#([^0-9])([a-z]|[0-9]|(_))*(?=(\s|$)*)/gi);
  if ( hashesFound != null )
  {
    // log(contentJson.config.self,'convertHashesToTags', contentJson.config.link_self, 'info', true);
    let theseTags = [];
    if (!contentJson.props.tags)
    {
      contentJson.props.tags = [];
    }
    const endHashGroupFound = text.match(/(\s)*(((#)([^0-9])([a-z]|[0-9]|(_))*)(\s|$))*$/gi);
    if ( endHashGroupFound != null )
    {
      let hashesFound = endHashGroupFound[0].match(/#([^0-9])([a-z]|[0-9]|(_))*(?=(\s|$)*)/gi);
      if ( hashesFound != null )
      {
        for ( let i=0; i<hashesFound.length; i++ )
        {
          const thisHash = hashesFound[i];
          const thisHashStored = thisHash.trim().toLowerCase();
          if (!theseTags.includes(thisHashStored))
            theseTags.push(thisHashStored);
        }
        let replacePosition = text.indexOf(endHashGroupFound[0]);
        let before = text.substring(0, replacePosition);
        let after = text.substring(replacePosition+endHashGroupFound[0].length);
        text = before + after;
      }
    }
    let hashesFound = text.match(/#([^0-9])([a-z]|[0-9]|(_))*(?=(\s|$)*)/gi);
    if ( hashesFound != null )
    {
      for ( let i=0; i<hashesFound.length; i++ )
      {
        const thisHash = hashesFound[i];
        const thisHashStored = thisHash.trim().toLowerCase();
        if (!theseTags.includes(thisHashStored))
          theseTags.push(thisHashStored);
        let replacePosition = text.indexOf(thisHash);
        let before = text.substring(0, replacePosition);
        let after = text.substring(replacePosition+thisHash.length);
        text = before + thisHash.substring(1) + after;
      }
    }
    contentJson.props.tags = theseTags;
  }

  return { 'contentText': text, 'contentJson': contentJson };
}

function convertImagePathsToHTML ( contentText )
{
  let text = contentText;

  // this regex catches all groups of image paths - even "groups" of 1 image path.
  const imageGroupsFound = text.match( /((\s)*(\/|(\.\/)|https?:)[^\s\r\n]+(\.png|\.jpg|\.gif)(?=(\s|$)))+/gmi );
  if ( imageGroupsFound != null )
  {
    for ( let i=0; i<imageGroupsFound.length; i++ )
    {
      let thisImageGroup = imageGroupsFound[i].trim();
      let imagePreview = '';
      // this regex captures all image paths individually
      const imagePathsFound = thisImageGroup.match( /(?<=(^|\s))([^\s"']+(\.png|\.jpg|\.gif)(?=($|\s)))/gmi );
      if ( imagePathsFound != null )
      {
        if (imagePathsFound.length==1)
        {
          imagePreview = '<div class="wrapper-image-alone"><img src="'+ imagePathsFound[0] +'" style="max-width:100%;height:auto;"></div>';
        }
        else
        {
          imagePreview = '<div class="wrapper-image-gallery"><ul>';
          for ( let i=0; i<imagePathsFound.length; i++ )
          {
            imagePreview += '<li><img src="'+ imagePathsFound[i] +'" loading="lazy"></li>';
          }
          imagePreview += '</ul></div>';
        }
      }
      else
      {
        log('no images found in '+thisImageGroup, 'convertImagePathsToHTML', contentText, 'err', true);
      }

      let replacePosition = text.indexOf(thisImageGroup);
      let before = text.substring(0, replacePosition);
      let after = text.substring(replacePosition+thisImageGroup.length);
      text = before + imagePreview + after;
    }

    return text;
  }
  else
  {
    return  contentText;
  }
}

async function convertPlaintextLinksToHTML ( contentText, contentJson )
{
  let text = contentText;
  const linksFound = text.match( /(?<=(^|\s))(https?)[^\s"']+(?=($|\s))/gm );
  const aLink = [];
  // log ( linksFound, 'convertPlaintextLinksToHTML', '', 'info');
  if ( linksFound != null )
  {
    let linkJson = JSON.parse(FS.readFileSync(path.join( PROJECT_PATH, 'src', 'data', 'links.json' )));
    for ( let i=0; i<linksFound.length; i++ )
    {
      linksFound[i] = linksFound[i].trim();
      let linkUrl = linksFound[i];
      if ( !( linksFound[i].match( /(http(s?)):\/\// ) ) )
      {
        linkUrl = 'http://' + linksFound[i];
      }
      const linkLengthLimit = 30;
      let linkText = linkUrl.slice(8);
      if (linkText[0]=='/')
          linkText = linkUrl.slice(1);
      if (linkText.length>linkLengthLimit)
        linkText = linkText.substring(0,linkLengthLimit)+'...';

      let linkData =
      {
        valid: false,
        local : false,
        url : '',
        title : '',
        description : '',
        image : '',
        player : '',
        site : '',
        imageSplash : false
      };
      let linkId =linkUrl;
      if (linkId.includes('#'))
      {
        linkId = linkId.split('#')[0];
      }
      // is this link indexed in 'links.json'?
      let linkIndex = linkJson.links.findIndex( linkRecord => linkRecord.url === linkId );
      if ( linkIndex > -1 )
      {
        linkData.valid = true;
        linkData.local = linkJson.links[linkIndex].local;
        linkData.url = linkJson.links[linkIndex].url;
        linkData.title = linkJson.links[linkIndex].title;
        linkData.description = linkJson.links[linkIndex].description;
        linkData.image = linkJson.links[linkIndex].image;
        linkData.site = linkJson.links[linkIndex].site;
        linkData.player = linkJson.links[linkIndex].player;
        linkData.imageSplash = linkJson.links[linkIndex].imageSplash;
      }

      // this is where we fetch the linkdata and update it in our json
      updateLinkUrlData ( linkUrl );

      if ( linkText.match( /www.youtu/ ) )
      {
        let youtubeID = '';
        if (linkData.player)
        {
          youtubeID = linkData.player.substring(linkData.player.lastIndexOf('/')+1);
        }
        else
        {
          youtubeID = linkUrl.substring(linkUrl.lastIndexOf('?v=')+3);
        }
        contentJson.props.player = 'https://www.youtube.com/embed/'+youtubeID+'?autoplay=1';

        let previewContent = '<div id="'+youtubeID+'" style="'+ (linkData.valid ? 'display:none;' : 'display:block;') +'" class="wrapper-video"><iframe src="https://www.youtube.com/embed/' + youtubeID + '?autoplay=1" loading="lazy" style="width:100%; height: 15rem;" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" tabindex="1" allowfullscreen></iframe></div>';

        if (linkData.valid)
        {
          let button = '<svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%" style="z-index:100; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); width:4rem;height:2.8rem;"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#f00"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg>';

          previewContent = '<div id="controller-'+youtubeID+'" class="click-blocker wrapper-video" style="display: block; position: relative; cursor: pointer;" tabindex="1" onClick="onClickToggleVideo(\''+youtubeID+'\')" onkeydown="onClickToggleVideo(\''+youtubeID+'\',event)">'+button+'<div style="width:100%; height: 15rem; background-image:url('+linkData.image+');background-size:cover;background-position:center center;"> </div></div>' + previewContent;
          if (!contentJson.props.head_og_image)
          {
            // we should parse out parameters if they have been given to the image

            contentJson.props.head_og_image = linkData.image;
          }

        }

        aLink.push( previewContent );
      }
      else if ( linkText.match( /soundcloud/ ) )
      {
        let soundcloudID = '';
        let parsedPlayerUrl = linkData.player.match(/([0-9]+(?=(&|\?)))/);

        if (parsedPlayerUrl)
        {
          soundcloudID = parsedPlayerUrl[1];
        }

        // disambiguate between a playlist and a single track
        let soundCloudSetType = 'tracks';
        if ( linkText.match( /\/sets\// ) )
        {
          soundCloudSetType = 'playlists';
        }

        let previewContent = linkText;
        if (linkData.valid && soundcloudID!='')
        {
          let button = '<svg height="306pt" preserveAspectRatio="xMidYMid meet" viewBox="0 0 501 306" width="100%" xmlns="http://www.w3.org/2000/svg" style="z-index:100; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); width:10rem;height:4.5rem;"><g transform="matrix(.1 0 0 -.1 0 306)"><path d="m2885 3050c-167-19-353-91-398-154l-22-31-3-965c-2-943-2-966 17-997 11-18 32-40 48-50 26-17 77-18 953-18h925l80 23c257 75 447 268 501 508 24 109 15 261-21 368-35 99-108 209-188 281-69 63-206 135-297 156-63 15-200 18-280 6l-56-8-23 78c-102 347-382 637-726 751-147 49-352 70-510 52z" fill="#f50" fill="#f50"/><path d="m2156 2688c-10-16-22-126-40-368-43-578-49-690-43-830 8-180 64-618 83-648 18-27 64-30 86-4 15 16 53 257 89 562 15 128 7 319-36 835-26 308-41 436-51 453-10 14-24 22-44 22s-34-7-44-22z" fill="#f50"/><path d="m1450 2544c-23-27-82-723-82-964-1-245 56-724 88-747 23-16 45-17 68 0 29 22 98 540 98 742 0 201-70 944-91 969-17 20-65 21-81 0z" fill="#f50"/><path d="m1807 2462c-13-14-23-87-46-344-52-558-52-580 5-1053 27-221 35-245 80-245 32 0 50 22 58 68 19 125 67 522 72 607 7 108-1 227-47 665-31 300-37 320-84 320-12 0-29-8-38-18z" fill="#f50"/><path d="m1111 2411c-21-21-22-30-56-361-45-451-44-555 10-999 26-205 35-231 81-231 13 0 30 10 38 23 22 34 96 597 96 727 0 74-19 285-56 604-25 228-34 256-76 256-10 0-26-8-37-19z" fill="#f50"/><path d="m407 2126c-13-14-40-162-68-381-15-115-8-275 27-580 19-171 31-245 42-257 18-22 55-23 72-3 11 14 56 340 80 587 9 98 9 136-5 231-26 195-63 387-75 402-16 19-54 19-73 1z" fill="#f50"/><path d="m762 2053c-16-14-27-52-47-163-41-217-43-367-10-682 14-142 30-280 34-309 10-61 29-84 65-75 34 8 39 31 70 306 53 461 53 468 0 745-30 156-31 161-50 180-20 20-37 19-62-2z" fill="#f50"/><path d="m81 1946c-17-20-61-238-68-336-4-64 1-133 21-275 33-233 40-262 67-270 38-12 49 16 81 196 42 238 43 369 5 534-16 66-35 130-43 143-16 25-46 29-63 8z" fill="#f50"/><path d="m391 462c-37-20-71-73-71-110 0-70 51-121 144-143 68-15 96-34 96-64 0-56-90-73-154-30l-37 25-29-22c-34-27-36-39-12-60 40-36 88-52 153-52 113 0 175 57 166 157-5 63-40 99-115 121-92 27-122 44-122 65 0 25 36 51 71 51 14 0 42-11 62-24l35-24 31 32c30 31 31 33 14 48-58 50-168 64-232 30z" fill="#f50"/><path d="m824 461c-123-75-139-311-27-415 63-59 184-54 249 10 98 98 80 335-30 399-54 32-145 35-192 6zm130-75c44-18 66-67 66-149 0-81-27-127-83-141-31-8-42-6-65 10-45 34-64 80-60 151 4 68 25 108 69 130 30 16 33 16 73-1z" fill="#f50"/><path d="m1202 313c3-151 5-173 26-215 52-105 194-128 278-44 45 45 54 89 54 269v157h-39-39l-4-159c-3-144-5-162-24-188-42-56-110-56-144 0-18 28-20 51-20 190v157h-46-46z" fill="#f50"/><path d="m1680 245v-235h40 39l3 152 3 153 96-150 95-150 42-3 42-3v235 236h-45-45v-151-152l-78 124c-122 194-109 179-153 179h-39z" fill="#f50"/><path d="m2160 245v-235h84c182 0 265 75 266 238 0 166-76 232-267 232h-83zm225 115c65-61 51-206-25-245-16-8-49-15-75-15h-45v146 146l60-4c45-3 66-10 85-28z" fill="#f50"/><path d="m2683 455c-106-64-131-270-46-383 64-85 197-93 270-15 18 19 33 38 33 43 0 4-15 17-33 28l-33 19-34-30c-27-24-39-28-68-23-62 10-102 71-102 157 0 40 29 108 55 129 28 23 90 18 118-9 24-22 26-22 58-6 19 10 35 19 37 20 8 7-33 57-63 75-48 30-137 27-192-5z" fill="#f50"/><path d="m3040 245v-235h155 155v45 45h-115-115v190 190h-40-40z" fill="#f50"/><path d="m3494 461c-123-75-139-311-27-415 63-59 184-54 249 10 98 98 80 335-30 399-54 32-145 35-192 6zm130-75c44-18 66-67 66-149 0-81-27-127-83-141-31-8-42-6-65 10-45 34-64 80-60 151 4 68 25 108 69 130 30 16 33 16 73-1z" fill="#f50"/><path d="m3872 313c3-151 5-173 26-215 52-105 194-128 278-44 45 45 54 89 54 269v157h-39-39l-4-159c-3-144-5-162-24-188-42-56-110-56-144 0-18 28-20 51-20 190v157h-46-46z" fill="#f50"/><path d="m4350 244v-236l103 4c85 3 109 8 144 28 83 46 124 148 103 257-24 135-95 183-267 183h-83zm221 120c29-25 49-75 49-119 0-88-45-137-130-143l-60-4v147 147l60-4c40-3 66-11 81-24z" fill="#f50"/></g></svg>';

          previewContent = '<div id="controller-sc'+soundcloudID+'" class="click-blocker wrapper-video" style="display: block; position: relative; cursor: pointer;" tabindex="1" onClick="onClickToggleVideo(\'sc'+soundcloudID+'\')" onkeydown="onClickToggleVideo(\'sc'+soundcloudID+'\',event)">'+button+'<div style="width:100%; height: 15rem; background-image:url('+linkData.image+');background-size:cover;background-position:center center;"> </div></div>' + '<div id="sc'+soundcloudID+'" style="display:none;" class="wrapper-video"><iframe scrolling="no" frameborder="no" tabindex="1" allow="autoplay" loading="lazy" style="width:100%; height: 15rem;" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/'+soundCloudSetType+'/'+soundcloudID+'&color=%230c1414&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe></div>';
          contentJson.props.player = 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/'+soundCloudSetType+'/'+soundcloudID+'&color=%230c1414&auto_play=true&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=false&visual=true';
          if (!contentJson.props.head_og_image)
          {
            contentJson.props.head_og_image = linkData.image;
          }
        }

        aLink.push( previewContent );
      }
      // else if ( linkText.match( /vimeo/ ) )
      // {
      //   let vimeoID = linkUrl.split( '/' ).slice(-1)[0];
      //   aLink.push( '<div class="wrapper-video"><iframe src="https://player.vimeo.com/video/' + vimeoID + '" style="width:100%; height:100%; min-height: 30vw; max-height: 10rem;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>' );
      // }
      else
      {
        let previewContent = linkText;

        if (linkData.valid)
        {
          let previewImage = '';
          if (linkData.image)
          {
            previewImage = '<img src="'+linkData.image+'" loading="lazy" style="max-width:100%;height:auto;">';
          }
          if (linkData.imageSplash)
          {
            previewContent = '<div class="wrapper-preview"><div class="wrapper-image-alone">'
                              +previewImage
                              +'<div class="preview-site'+ (linkData.local?' purple':'')+'">'+linkData.site+'</div>'
                              +'</div>'
                              +'<div class="preview-text"><div class="preview-title">'+ linkData.title+'</div><div class="preview-description">'+linkData.description+'</div></div></div>';
          }
          else
          {
            previewContent = '<div class="wrapper-preview" style="display:flex;"><div class="wrapper-image-alone" style="width:20%;">'+previewImage+'</div><div class="preview-text" style="width:80%;"><div class="preview-title">'
            + linkData.title+'</div><div class="preview-description">'
            + linkData.description + '</div></div></div>';
          }
        }

        let linkPreview = '<a href="' + linkUrl + '" tabindex="1"';
        if (!linkData.local)
            linkPreview+= ' target="new"';
        linkPreview    += '>' + previewContent + '</a>';

        aLink.push( linkPreview );
      }

      text = text.split( linksFound[i] ).map(item => { return aLink[i].includes('iframe') ? item.trim() : item; } ).join( aLink[i] );
    }
    return { 'contentText': text, 'contentJson': contentJson };
  }
  else
  {
    return { 'contentText': contentText, 'contentJson': contentJson };
  }
}

function convertUnusualCharactersToHTMLEntities ( contentText)
{
  contentText = contentText.replaceAll('’','&CloseCurlyQuote;');
  contentText = contentText.replaceAll('”','&CloseCurlyDoubleQuote;');
  return contentText;
}

function encodeHtmlEntities( rawString='' )
{
  if (rawString!='')
    return rawString.replaceAll(/[\u00A0-\u9999<>\&]/g, (i) => '&#'+i.charCodeAt(0)+';');
  else
    return rawString;
}
/**
 * This function is for a single Json file - can be called from stream
 * @param {*} props - content JSON .props
 * @param {*} sourcePath - file path of the HTML file
 * @returns string. URL to RSS
 */
function createRSSfileFromList (props, sourcePath)
{
  let rssFilePath     = sourcePath.replace('.html','.rss');
  let rssAddress      = SITE.url + props.link_self + "/" + path.basename(rssFilePath);
  let rssList         = sortContentList(props.list_content);

  // header
  let rssFileContents  ='<?xml version="1.0"?>\n'
                      + '<rss version="2.0"><channel>\n'
                      + '<link>'+ SITE.url + props.link_self +'</link>\n'
                      + '<title>'+ encodeHtmlEntities( props.head_title ) +'</title>\n'
                      + '<description>'+ encodeHtmlEntities( props.head_description ) +'</description>\n'
                      + '<language>en</language>\n'
                      ;
  // loop through the rssList
  rssList.forEach ( (rssItem) =>
  {
    rssFileContents   +='<item>\n'
                      + '<title>'+ encodeHtmlEntities( rssItem.props.head_title ) +'</title>\n'
                      + '<description>'+ encodeHtmlEntities( rssItem.props.head_description ) +'</description>\n'
                      + '<link>'+ SITE.url + rssItem.props.link_self +'</link>\n'
                      + '</item>\n'
                      ;
  });
  // footer
      rssFileContents +='</channel></rss>\n';

  // write file
  FS.writeFileSync(rssFilePath, rssFileContents);

  return rssAddress;
}

/**
 * This function is for a single Json file - can be called from stream
 * @param {*} file - Vinyl object. JSON file.
 * @param {*} watchedChange
 * @returns Vinyl object. HTML file.
 */
function compileJsonToFile (file, watchedChange)
{
  handlebarsRegisterAllPartials();

  let dataJson = JSON.parse(file.contents.toString());
  const template = getTemplateForContent(dataJson);

  if (template)
  {
    // Generate values which are specific to development or production
    // get the canonical URL to this file
    let newFilePath = '';
    let localUrlSite = '';
    if (dataJson.config.link_self)
    {
      localUrlSite = dataJson.config.link_self;
    }
    else
    {
      // this is an index.json without content so...
      // we need to establish the URL to this page
      const relPath = path.relative(srcContentBasePath, file.path);
      const relDirPath = path.dirname(relPath);
      const localUrlBase = '/' + relDirPath.replaceAll('\\','/');
      if (localUrlBase!=='/.')
        localUrlSite = localUrlBase;
      else
      {
        const basename = path.basename(file.path, path.extname(file.path));
        if (basename==='index')
          localUrlSite = '/';
        else
          localUrlSite = '/'+basename;
      }
    }
    // set the URL for sharing in social media
    dataJson.props.head_og_url = SITE.url + localUrlSite;

    // file path
    if (dataJson.config.type==='status')
    {
      let baseName = dataJson.props.id;
      // this is a weird exception
      if (baseName==undefined || !baseName){ baseName = 'index'; }

      // if (dataJson.config.flatten_child_directories)
      // {
      //   const list_path = path.dirname(dataJson.config.list_path);
      //   // path.relative()
      // }
      // else
      // {
        newFilePath = path.join(path.dirname(file.path), baseName+'.html');
      // }
      if (dataJson.config.flatten_child_directories && dataJson.config.list_path)
      {
        let pathString = file.path.toString();
        let list_dirname = path.dirname(dataJson.config.list_path);
        let endPos = pathString.indexOf(list_dirname);
        if (endPos>-1)
          endPos = endPos + list_dirname.length;
        newFilePath = path.join( pathString.substring(0,endPos), baseName+'.html');
      }
    }
    else
    {
      newFilePath = file.path.replace('.json','.html');
      if (dataJson.config.flatten_child_directories && dataJson.config.list_path)
      {
        const basename = path.basename(file.path, path.extname(file.path));
        let pathString = file.path.toString();
        let list_dirname = path.dirname(dataJson.config.list_path);
        let endPos = pathString.indexOf(list_dirname);
        if (endPos>-1)
          endPos = endPos + list_dirname.length;
        newFilePath = path.join( pathString.substring(0,endPos), basename+'.html');
      }
    }

    // need to parse this from
    if (dataJson.props.head_og_image )
    {
      if (dataJson.props.head_og_image.charAt[0]=='/')
        dataJson.props.head_og_image = SITE.url + dataJson.props.head_og_image;
    }
    else if (dataJson.props.background_image)
    {
      // contentJson.props.background_image = SITE.url + contentJson.props.background_image;
      if (dataJson.props.background_image.charAt[0]=='/')
        dataJson.props.head_og_image = SITE.url + dataJson.props.background_image;
    }

    // what the fuck?
    dataJson.props.id = dataJson.props.id;

    // digests are a list collecting the content from other lists
    if (dataJson.config.digest_lists)
    {
      if (!dataJson.props.list_content) { dataJson.props.list_content = []; }
      let digestLists = dataJson.config.digest_lists;
      digestLists.forEach( (list)=>
      {
        let this_list_path = path.join(PROJECT_PATH, list.path);
        if (FS.existsSync(this_list_path))
        {
          let thisJson = JSON.parse(FS.readFileSync(this_list_path));
          if (thisJson.props.list_content)
          {
            let sort = '';
            if (list.sort) { sort = list.sort; }
            thisJson.props.list_content = sortContentList(thisJson.props.list_content, sort);
            for (let i=0; i<list.number; i++)
            {
              let list_item = thisJson.props.list_content[i];
              list_item.props.list_count = thisJson.props.list_content.length;
              dataJson.props.list_content.push(list_item);
            }
          }
        }
      });
    }

    // list content
    if (   dataJson.props.list_content
        && dataJson.props.list_content.length>1
       )
    {
      // we should create an RSS feed for it
      let rssAddress = createRSSfileFromList(dataJson.props, newFilePath);

      dataJson.props.rss = rssAddress;

      // and sort it
      let sort = '';
      if (dataJson.config.sort) { sort = dataJson.config.sort; }
      dataJson.props.list_content = sortContentList(dataJson.props.list_content, sort);
    }

    // shortening the list if there is a limit
    let listMax = dataJson.config.list_max;
    if (listMax>0 && dataJson.props.list_content.length > listMax)
    {
      dataJson.props.list_content.length = listMax;
    }

    // set develpoment bit to allow different rendering for dev builds
    // log( 'DESTINATION_DIR_NAME('+DESTINATION_DIR_NAME+')', 'compileJsonToFile', '', 'info', true);
    if (DESTINATION_DIR_NAME == 'www_dev')
    {
      dataJson.props.development = true;
    }
    else
    {
      dataJson.props.development = false;
    }
    
    // NOW WRITE THE HTML using the TEMPLATE
    // get the template
    const thisHandlebarsTemplate = Handlebars.compile(template);
    // console.log(dataJson.config);
    // console.log(dataJson.props);
    const htmlCompiledFromHandlebars = thisHandlebarsTemplate(dataJson.props);
    // adjust the Vinyl object before pushing it back to the stream
    // - change path as needed
    file.path = newFilePath;
    // - update the contents with the compiled html
    file.contents = Buffer.from(htmlCompiledFromHandlebars);
  }
  // this is a json file which was used by the site but does not have a template
  // we're going to send back a null so that nothing gets deployed
  else if (dataJson.config && dataJson.props)
  {
    file = null;
  }
  return file;
}

/**
 * A stream of all JSON files are piped through here
 * @param {*} callback
 * @param {*} watchedChange
 * @returns stream for writing
 */
function compileDataToHtml (callback, watchedChange)
{
  var stream = through.obj(function (file=new Vinyl(), enc, callBack)
  {
    // we should only do this if we are going to publish it
    if (shallPublish (JSON.parse(file.contents)) )
    {
      const htmlFile = compileJsonToFile(file, watchedChange);

      // make sure the file goes through the next gulp plugin
      if (htmlFile)
      {
        this.push(htmlFile);
      }
    }

    // tell the stream engine that we are done with this file
    callBack();
  });

  // returning the file stream
  return stream;
}

///////////////////////////////////////


function shallPublish (contentJson)
{
  let publish_this = false;
  if (!contentJson.config)
    return false;
  let this_file_name = contentJson.config.contentPath ? path.basename(contentJson.config.contentPath) : '';
  // if the date_published is more recent than today
  if (    DESTINATION_DIR_NAME=='www_dev'
      || ( contentJson.config.date_published &&  contentJson.config.date_published <= (new Date().toJSON()) )
      || this_file_name == ''
     )
  {
    publish_this = true;
  }
  else
  {
    log('NOT PUBLISHING Publish('+contentJson.config.date_published+') Now('+(new Date().toJSON())+')', 'shallPublish', this_file_name, 'info');
  }

  return publish_this;
}

function publishContentToLists (originalContentJson, siteJson, file)
{
  // CONTENT LIST(s) ----------------------------------------
  // during a build: only add published items to a list
  if (shallPublish (originalContentJson))
  {
      // RECORD ------ of this content for insertion into lists ------

    // create a copy
    let contentJson = JSON.parse(JSON.stringify(originalContentJson));

    // we use this to sort the content in a list.
    // and we want to be able to sort content which is not published when we are in development
    let sortByDate = '';
    if (contentJson.config.date_published)
      sortByDate = contentJson.config.date_published;
    else
      sortByDate = contentJson.config.date_modified;
    let sortType = '';
    if (contentJson.config.content_type)
      sortType = contentJson.config.content_type;
    else
      sortType = contentJson.config.type;

    // this is generic - and should be usable by other kinds of content
    let thisRecord =
    {
      date: sortByDate,
      type: sortType,
      id: contentJson.props.id,
      parent_id: contentJson.config.parent_id,
      background_image: contentJson.props.background_image ? contentJson.props.background_image : '',
      props: contentJson.props
    };

    const thisContentIndex = siteJson.content.findIndex( contentRecord => contentRecord.id===contentJson.props.id );
    if (siteJson.content[thisContentIndex] && siteJson.content[thisContentIndex].isParent)
    {
      thisRecord.props.list_content = true;
    }

    // we'll build a list of all the lists we are going to publish this content to
    let publishListPaths = [];
    // this content is listed on other pages. we convert this content to a record then insert it in other lists
    //  - listJsonPath - path to the master list at start
    //                   we change the list at the bottom of the loop
    const masterIndexPath = contentJson.config.list_path;
    const thisDirectoryIndexPath = path.join(path.dirname(file.path), 'index.json');
    // master list?
    if ( masterIndexPath!==thisDirectoryIndexPath )
      publishListPaths.push(masterIndexPath);
    // this directory?
    if ( !path.basename(file.path).includes('index') )
      publishListPaths.push(thisDirectoryIndexPath);
    // parent list?
    if ( contentJson.config.parent_id )
    {
      // pull the json path of the parent from siteJson
      const parentContentIndex = siteJson.content.findIndex( contentRecord => contentRecord.id===contentJson.config.parent_id );
      if (parentContentIndex>-1)
      {
        siteJson.content[parentContentIndex].isParent = true;
        publishListPaths.push(siteJson.content[parentContentIndex].json_path);
      }
    }
    // publish to other lists?
    if ( contentJson.config.list_publish )
      publishListPaths = publishListPaths.concat(contentJson.config.list_publish);

    log(publishListPaths, 'publishContentToLists', file.path, 'info');
    publishListPaths.forEach( (thisListJsonPath) =>
    {
      thisListJsonPath = path.join(PROJECT_PATH, thisListJsonPath);
      if ( FS.existsSync(thisListJsonPath) )
      {
        // initialization
        let thisContentList = [];
        let listJson = JSON.parse(FS.readFileSync(thisListJsonPath, 'utf8'));
        if (listJson.props.list_content) { thisContentList = listJson.props.list_content; }

        // INSERTION ----- of content into the list ----------------------
        if (thisContentList.length>0)
        {
          const recordIndex = thisContentList.findIndex( listRecord => listRecord.id===thisRecord.id );
          if (recordIndex>-1) { thisContentList[recordIndex] = thisRecord; } // replace
          else                { thisContentList.unshift(thisRecord); } // push into index 0
        }
        else
        {
          thisContentList.push(thisRecord);
        }
        // update site json tracking that this resource has children
        const parentContentIndex = siteJson.content.findIndex( contentRecord => contentRecord.id===listJson.props.id );
        if (siteJson.content[parentContentIndex])
          siteJson.content[parentContentIndex].isParent = true;

        listJson.props.list_content = thisContentList;

        // WRITE the changes --------
        FS.writeFileSync(thisListJsonPath, JSON.stringify(listJson, null, 2));
      } // if file exists end
    });// foreach end
  } // CONTENT LIST(s) end ----------------------------------------------
  // we return stringified siteJson for comparison to determine if it was changed
  return JSON.stringify(siteJson, null, 2);
}

///////////////////////////////////////

async function convertImageToData (contentJson, extType, file)
{
  // log('','convertImageToData', file.path, 'info', true);
  // get file base name
  let imageName = path.basename(file.path, path.extname(file.path));
  let imageURL = '/' + path.relative(srcContentBasePath, file.path).replaceAll('\\','/'); // absolute path
  let resizeDirPath  = path.join(path.dirname(file.path),'resize');
  let imagePathTiny = path.join(resizeDirPath,imageName+'-144w.jpg');
  let imageURLTiny = '/' + path.relative(srcContentBasePath, imagePathTiny).replaceAll('\\','/'); // absolute path
  let imagePathXSmall = path.join(resizeDirPath,imageName+'-250w.jpg');
  let imageURLXSmall = '/' + path.relative(srcContentBasePath, imagePathXSmall).replaceAll('\\','/'); // absolute path
  let imagePathSmall = path.join(resizeDirPath,imageName+'-360w.jpg');
  let imageURLSmall = '/' + path.relative(srcContentBasePath, imagePathSmall).replaceAll('\\','/'); // absolute path
  let imagePathMobile = path.join(resizeDirPath,imageName+'-450w.jpg');
  let imageURLMobile = '/' + path.relative(srcContentBasePath, imagePathMobile).replaceAll('\\','/'); // absolute path

  // IMAGE METADATA
  if (!contentJson.props.head_title)
    contentJson.props.head_title = imageName;
  if (!contentJson.props.head_description)
    contentJson.props.head_description = '';
  if (!contentJson.props.head_og_type)
    contentJson.props.head_og_type = 'image';
  contentJson.props.head_og_image = imageURL;
  contentJson.props.background_image = '';

  if (!contentJson.props.head_og_image_width || !contentJson.props.head_og_image_height || FORCE_CONTENT_PROCESSING)
  {
    const metadata = await sharp(file.path).metadata();
    // create date is set to the actual image create date
    //    unless it has been adjusted by the user
    let date_today = new Date();
    let configDate = new Date(contentJson.config.date_created);
    if (  !contentJson.config.date_created ||
        (     date_today.getFullYear() == configDate.getFullYear()
          &&  date_today.getMonth() == configDate.getMonth()
          &&  date_today.getDay() == configDate.getDay()
        )
      )
    {
      // getting create date
      if (metadata.xmp)
      {
        const xmp = metadata.xmp.toString();
        let startPos  = xmp.indexOf('xmp:CreateDate');
        // 2023-04-30T17:24:08
        if (startPos>-1)
        {
          startPos  +='xmp:CreateDate'.length;
          let maxPos = startPos+19;
          let midPos = xmp.indexOf('T', startPos);
          if (midPos!=-1 && midPos<maxPos)
          {
            startPos = midPos - 10;
            contentJson.config.date_created = new Date(xmp.substring(startPos, startPos+19)).toISOString();
            contentJson.config.date_modified = contentJson.config.date_created;
          }
        }
        else
        {
          log('missing xmp:CreateDate: '+JSON.stringify(xmp),'convertImageToData', file.path, 'err', true);
        }
      }
      else
      {
        log('missing XMP in metadata: '+JSON.stringify(metadata),'convertImageToData', file.path, 'err', true);
      }
    }

    contentJson.props.head_og_image_width   = metadata.width;
    contentJson.props.head_og_image_height  = metadata.height;
  }

  // create the directory for all resized images if it does not exist
  if (!FS.existsSync(resizeDirPath)) { FS.mkdirSync(resizeDirPath); }
  // if resized images do not exist, create them
  if (contentJson.props.head_og_image_width>144 && !FS.existsSync(imagePathTiny))
  {
    sharp(file.path).resize({ width:144 }).toFormat('jpg').toFile(imagePathTiny);
  }
  if (contentJson.props.head_og_image_width>360 && !FS.existsSync(imagePathXSmall))
  {
    // this was a new filesize and so... saving myself some headaches here
    if (FS.existsSync(imagePathSmall))
      sharp(imagePathSmall).resize({ width:250 }).toFormat('jpg').toFile(imagePathXSmall);
    else
      sharp(file.path).resize({ width:250 }).toFormat('jpg').toFile(imagePathXSmall);
  }
  if (contentJson.props.head_og_image_width>360 && !FS.existsSync(imagePathSmall))
  {
    sharp(file.path).resize({ width:360 }).toFormat('jpg').toFile(imagePathSmall);
  }
  if (contentJson.props.head_og_image_width>450 && !FS.existsSync(imagePathMobile))
  {
    sharp(file.path).resize({ width:450 }).toFormat('jpg').toFile(imagePathMobile);
  }
  // this is where we should setup srcset etc...
  contentJson.props.content = '<img id="'+contentJson.props.id+'" src="'+ imageURL +'" srcset="'+imageURLMobile+' 450w, '+ imageURL +' '+contentJson.props.head_og_image_width+'w" sizes="(max-width: 900px) 450px, (min-width: 901px) 100vw" title="'+contentJson.props.head_title+'" alt="'+contentJson.props.head_title+'" loading="lazy">';
  contentJson.props.content_synopsis = '<img id="'+contentJson.props.id+'" srcset="'+imageURLTiny+' 144w, '+imageURLXSmall+' 250w, '+imageURLSmall+' 360w" sizes="(max-width: 737px) 144px, (min-width: 738px) 250px, (min-width: 1501px) 360px" title="'+contentJson.props.head_title+'" alt="'+contentJson.props.head_title+'" loading="lazy">';
  // contentJson.props.content_synopsis = '<img id="'+contentJson.props.id+'" src="'+imageURLSmall+'" alt="'+contentJson.props.head_title+'" loading="lazy">';
  return Promise.resolve(contentJson);
}

////////////////////////////////////////////

async function convertTextToData (contentJson, extType, file)
{
  log( 'contentJson length('+JSON.stringify(contentJson).length+')', 'convertTextToData', file.path, 'info');
  // get the raw content
  const contentText = file.contents.toString();

  // --SPECIAL PROCESSING OF CONTENT AND JSON ---------------------------------------------------------------------

  contentJson.props.head_og_type = 'article';

  // PROCESSING CONTENT for DISPLAY (converting to HTML things like links etc....)
  // process the content that we display here

  // first convert the plain text URLS to links
  // and then assign this new value to content.
  let tempContent = contentText;

  // count characters
  let contentIsALongFormPiece = false;
  if (tempContent.length > 500 && contentJson.config.type!='status')
  {
    contentIsALongFormPiece = true;
  }

  // count words
  let words = tempContent.split(/\s+/);
  let count = 0;
  for (let x=0; x<words.length; x++)
  {
    if( words[x].match(/[a-z]/gi)!==null )
    {
      count++;
    }
  }
  contentJson.props.words = count;

  // this creates fixed width tabs out of two spaces at the start of a line
  // special to subterfugue - not a universal thing
  if (contentJson.props.status_class!='text-ascii-art')
  {
    if (tempContent.slice(0, 2)=='  ')
      tempContent = '<span class="indent"> </span>' + tempContent.substring(2);
    tempContent = tempContent.replaceAll('\n  ','\n<span class="indent"> </span>');
  }

  if (extType=='.txt')
  {
    let hashResult = convertHashesToTags(tempContent, contentJson);
    tempContent = hashResult.contentText;
    contentJson = hashResult.contentJson;
    tempContent = convertImagePathsToHTML(tempContent);
    let linkResult = await convertPlaintextLinksToHTML(tempContent, contentJson);
    tempContent = linkResult.contentText;
    contentJson = linkResult.contentJson;
  }
  else if (extType=='.md')
  {
    tempContent = convertMarkdownToHtml(tempContent);
  }
  tempContent = convertUnusualCharactersToHTMLEntities(tempContent);

  // end content text processing
  contentJson.props.content = tempContent;

  let contentType = contentJson.config.content_type;
  if (!contentType)
      contentType = contentJson.config.type;
  let contentCategory = SITE.name+' &#9737; '+contentType;

  delete contentJson.props.content_synopsis;
  // synopsis of content (like a link preview) for status panes etc...
  if (
    (contentIsALongFormPiece || contentJson.props.content_synopsis_force)
    // && (!contentJson.props.content_synopsis || contentJson.props.content_synopsis=='')
  )
  {
    // this is a flag to hide the category elsewhere in a template since we are showing it in the link
    contentJson.props.content_category_hide = true;
    contentJson.props.content_synopsis = '';

    let previewImage = '';
    if (contentJson.props.head_og_image || contentJson.props.background_image)
    {
      let img_splash = contentJson.props.head_og_image;
      if (!img_splash || img_splash=='undefined')
          img_splash = contentJson.props.background_image;
      if (img_splash.length)
        previewImage = '<img src="'+img_splash+'" loading="lazy" style="max-width:100%;height:auto;">';
    }

    if (previewImage == '' || contentJson.props.content_synopsis_small)
    {
      contentJson.props.content_synopsis = '<div style="position:relative;"><div class="preview-site purple">'+contentCategory+'</div></div>'
      + '<a href="'+contentJson.props.link_self+'"><div class="wrapper-preview" style="display:flex;"><div class="preview-image" style="width:20%;">'+previewImage+'</div>'
      + '<div class="preview-text" style="width:80%;"><div class="preview-title">' + contentJson.props.head_title+'</div>'
      + '<div class="preview-description">'+ contentJson.props.head_description + '</div></div></div></a>';
    }
    else
    {
      contentJson.props.content_synopsis = '<a href="'+contentJson.props.link_self+'"><div class="wrapper-preview"><div class="wrapper-image-alone">'
                        +previewImage
                        +'<div class="preview-site purple">'+contentCategory+'</div>'
                        +'</div>'
                        +'<div class="preview-text"><div class="preview-title">'+ contentJson.props.head_title+'</div><div class="preview-description">'+contentJson.props.head_description+'</div></div></div></a>';
    }
  }
  else
  {
    // if (contentType!='fiction' && contentType!='pangram')
    //   delete contentJson.props.content_category;
  }

  if (contentJson.props.content.length>750 && extType=='.txt')
    contentJson.props.content_class = 'text-whitespace';

  // log( 'contentJson length('+JSON.stringify(contentJson).length+')', 'convertTextToData', '', 'info');

  return Promise.resolve(contentJson);
}

/**
 * Every content file that we want metadata for should pass through this function
 * fileType is used for handling specific content types
 * @param {*} file
 * @param {*} fileType - 'text' for text files, 'image' for images
 * @param {*} watchedChange
 * @returns altered contentJson created from the file
 */
async function convertFileToData (file, fileType, watchedChange)
{
  log('','convertFileToData ', file.path, 'info');
  let fileExt = path.extname(file.path);
  // Look for a json file full of data to be used in the page
  let contentJson = getContentJsonForFile (file, fileExt, watchedChange);
  // ? can we proceed?
  // Only process the text file if a template and type are defined
  if (    contentJson.config.template && contentJson.config.template!=='' && contentJson.config.template!==null
      &&  contentJson.config.type
    )
  {
    // ----- SITE JSON tracking all content ------
    // we keep a record of all content by id which allows us to do lookups by id (especially the json path)
    let siteJson = JSON.parse(FS.readFileSync(path.join(PROJECT_PATH,'src', 'data', 'site.json')));
    const contentIndex = siteJson.content.findIndex( contentRecord => contentRecord.id===contentJson.props.id );
    let originalContentRecord = {};
    let newContentRecord = {};
    if (contentIndex>-1)
    {
      originalContentRecord = siteJson.content[contentIndex];
      newContentRecord = siteJson.content[contentIndex];
      if (!newContentRecord.json_path || newContentRecord.json_path=='')
        newContentRecord.json_path =  contentJson.config.self;
    }
    // initialize
    else
    {
      newContentRecord = { id: contentJson.props.id, json_path: contentJson.config.self };
    }
    // ---------------------


    // --------------------------------------------------------------
    // SPECIAL HANDLING FOR DIFFERENT CONTENT TYPES
    // we transform the contentJson from the data we extract from the file
    if (contentJson.config.content_skip_processing && !FORCE_CONTENT_PROCESSING)
    {
      log('content processing skipped','convertFileToData ', file.path, 'info');
    }
    else
    {
      if (   !FORCE_CONTENT_PROCESSING
          && fileType !== 'image'
        )
      {
        // update date_modified here
          contentJson.config.date_modified = new Date().toJSON();
      }

      if (fileType==='text')
      {
        contentJson = await convertTextToData (contentJson, fileExt, file);
      }
      else if (fileType==='image')
      {
        contentJson = await convertImageToData (contentJson, fileExt, file);
      }
    }
    // -------------------------------------------------------------
    // NOW record the changes we made to contentJson back to the file

    // CONDITIONS FOR PUBLISHING
    // We only publish on a build
    // the value for config.date_published indicates when a thing can be published.
    // an invalid value like NULL or a non timestamp string keeps the file private
    // transfer config.date_modified to config.date_published
    if (    DESTINATION_DIR_NAME == 'www'
        && !contentJson.config.date_published
        &&  contentJson.config.date_modified
       )
    {
      contentJson.config.date_published = contentJson.config.date_modified;
      // we should set the date property here
      if ( !contentJson.props.copyright_year )
        contentJson.props.copyright_year = getCopyrightYear(contentJson.config.date_published);
      contentJson.props.date = getDateString(contentJson.config.date_published);
    }
    writeContentJsonFile(file, JSON.parse(JSON.stringify(contentJson, null, 2)));
    // we could write to an SQLite db here


    // ----------
    // changes to contentJson after this point will NOT be written to the file

    // record changes back to siteJson if there have been any
    let writeSiteJson = false;
    if (contentIndex==-1)
    {
      siteJson.content.push(newContentRecord);
      writeSiteJson = true;
    }
    // like comparing file hashes right?
    else if (JSON.stringify(newContentRecord)!==JSON.stringify(originalContentRecord))
    {
      siteJson.content[contentIndex] = newContentRecord;
      writeSiteJson = true;
    }

    // ----------------------------------------------------
    let originalSiteJson = JSON.stringify(siteJson, null, 2);
    let newSiteJson = publishContentToLists (contentJson, siteJson, file);

    if (  writeSiteJson ||  newSiteJson !== originalSiteJson )
    {
      FS.writeFileSync(path.join(PROJECT_PATH, 'src','data','site.json'), newSiteJson);
    }

  }
  return file;
}

/**
 * When we make a copy of an image we alter the path name to reflect this
 * This function looks for images that have been copied and altered in this way
 * @param {*} file
 * @returns true if this image is a copy of another image
 */
function getImageIsCopy (file)
{
  let isACopy = false;
  let extension = path.extname(file.path);
  let imageName = path.basename(file.path, extension);

  // we use - at the end of an image's file name to append identifiers of alternate versions
  // we should NEVER use - in an image's file name otherwise
  if (imageName.lastIndexOf('-')>-1)
  {
    // we could at this point look for the copy...
    // but add that later if this becoems a problem
    isACopy = true;
  }

  return isACopy;
}

function processImageinStream (callBack, watchedChange=false)
{
  // console.log('processImageinStream');

  // Create a stream through which each image passes
  var stream = through.obj(function (file=new Vinyl(), enc, callBack)
  {
    // we should ignore all resized images
    if (!getImageIsCopy(file))
    {
      file = convertFileToData(file, 'image', watchedChange);
    }

    // make sure the file goes through the next gulp plugin
    this.push(file);

    // tell the stream engine that we are done with this file
    callBack();
  });

  // returning the file stream
  return stream;
}

function processTextinStream (callBack, watchedChange=false)
{
  // console.log('processTextinStream');

  // Create a stream through which each text passes
  var stream = through.obj(function (file=new Vinyl(), enc, callBack)
  {
    file = convertFileToData(file, 'text', watchedChange);

    // make sure the file goes through the next gulp plugin
    this.push(file);

    // tell the stream engine that we are done with this file
    callBack();
  });

  // returning the file stream
  return stream;
}

function convertMarkdownToHtml (markdown)
{
  const converter = new Showdown.Converter();
  const html      = converter.makeHtml(markdown);

  return html;
}

/**
 * can be useful for converting html (after conversion from markdown into plain text for a description)
 * @param {*} html
 * @returns
 */
function convertHtmlToPlaintext (html)
{
  var plaintext = String(html).replace(/<[^>]+>/g, '');

  return plaintext;
}


/**
 * Compiles Handlebars in the src/content tree. These are the .hbs for unique Pages.
 * not compiled here are the layouts in Templates common to content.
 * @param {*} callBack
 * @param {*} watchedChange
 * @returns stream from through2
 */
function compileHandlebarsfromStream (callBack, watchedChange=false)
{
  // console.log('compileHandlebarsfromStream');
  // Register Partials
  // handlebarsRegisterAllPartials();

  // Create a stream through which each HBS will pass
  var stream = through.obj(function (file=new Vinyl(), enc, callBack)
  {
    // Look for a json file full of data to be used in the page
    // - get that path of the possible file
    const dataPath = file.path.replace('.hbs', '.json');
    let hbsJson = {};
    // - does the matching json file exist?
    if (FS.existsSync(dataPath))
    {
      // - it exists, let's grab that data
      const JSONString = FS.readFileSync(dataPath, 'utf8');
      hbsJson = JSON.parse(JSONString);

      // let type = hbsJson.config.type;
      // let list_type = '';

      // is this an INDEX of its directory?
      if ( path.basename(file.path) === 'index.hbs' )
      {
        // an index page is implicitly a list.
        // the type of an index page
        //    indicates what type of content is in its list,
        //    and thus the appropriate content files in its directory add themselves to its list
        //    while also inheriting type from the index file (unless this is manually changed)
        // BUT if the type is 'list'
        //    then this is a list of lists
        //    meaning that it probably has a list of references to other lists
        // Anyway....

        // let us sort this list
        if ( hbsJson.props.list_content )
        {
          // sort by date
          hbsJson.props.list_content.sort((a, b) =>
          {
            if (a.date < b.date)
            {
              return 1; // sort a after b
            }
            else if (a.date > b.date)
            {
              return -1; // sort a before b
            }
            else
            {
              return 0; // keep original order
            }
          });

          if ( hbsJson.props.list_content.length>=100 )
          {
            // long lists need to be broken down into sublists

          }

          // should rewrite the json after sorting
          FS.writeFileSync(dataPath, JSON.stringify(hbsJson, null, 2));
        }
      }
    }

    // compile HTML from handlebars, and apply the data to it
    const thisHandlebarsTemplate = Handlebars.compile(file.contents.toString());
    const htmlCompiledFromHandlebars = thisHandlebarsTemplate(hbsJson.props);

    // adjust the Vinyl object before pushing it back to the stream
    // - change extension to .html from .hbs
    file.path = file.path.replace('.hbs','.html');
    // - update the contents with the compiled html
    file.contents = Buffer.from(htmlCompiledFromHandlebars);

    // make sure the file goes through the next gulp plugin
    this.push(file);

    // tell the stream engine that we are done with this file
    callBack();
  });

  // returning the file stream
  return stream;
}

/**
 * this works for .json paired with .hbs pages but not content files
 * for it to work with content files we need to check for paired content, not a paired template
 * @param {*} lastUnlinkedJsonPath
 * @returns true or false depending on test
 */
function isLastUnlinkedJsonSolitary (lastUnlinkedJsonPath)
{
  if ( FS.existsSync(lastUnlinkedJsonPath) )
  {
    const JSONString = FS.readFileSync(lastUnlinkedJsonPath, 'utf8');
    let hbsJson = JSON.parse(JSONString);

    if ( !FS.existsSync(path.join(PROJECT_PATH, hbsJson.template)) )
    {
      return true; // if the template in the unlinked JSON does not exist... then we found the right file to rescue
    }
  }
  return false;
}

/**
 * generates .json for an .hbs page when first created
 * @param {*} thisPath - path of the .hbs file
 * @param {*} lastUnlinkedJsonPath - checking if we need to repair with this file
 */
function createDummyJsonDataForHandlebars (thisPath, lastUnlinkedJsonPath)
{
  if (FS.existsSync(thisPath.replace('.hbs','.json')))
  {
    // do not overwrite an existing .json file
    return;
  }
  // create a new .json
  let date_created = new Date();
  let defaultJson = `{
  "config":
  {
    "template": "${thisPath}",
    "date_created": "${date_created}"
  },
  "props":
  {
  }
}`;
  if ( lastUnlinkedJsonPath!=='' && isLastUnlinkedJsonSolitary(lastUnlinkedJsonPath) )
  {
    const JSONString = FS.readFileSync(lastUnlinkedJsonPath, 'utf8');
    let hbsJson = JSON.parse(JSONString);
    hbsJson.config.template = thisPath;
    defaultJson = JSON.stringify(hbsJson, null, 2);
    FS.rmSync(lastUnlinkedJsonPath);
  }
  else
  {
    const defaultPath = path.join('src','templates','defaultPageSchema.json');
    // - does the matching json file exist?
    if (FS.existsSync(defaultPath))
    {
      // - it exists, let's grab that data
      const JSONString = FS.readFileSync(defaultPath, 'utf8');
      let hbsJson = JSON.parse(JSONString);
      hbsJson.config.template = thisPath;
      hbsJson.config.date_created = date_created;

      defaultJson = JSON.stringify(hbsJson, null, 2);
    }
  }

  FS.writeFileSync(thisPath.replace('.hbs','.json'), defaultJson);
}

// --------------------------------------------------------------------

// --------------------------------------------------------------------
// Tasks - Initialization
// When you need to process everything
// also useful for builds

/** wipeLists
 *  Content lists and hashlists are generated when content is compiled
 * But when content is deleted the stale records remain in these lists
 * Solution here is to wipe all of them, and rebuild them
 */
function wipeLists ( )
{
  return src( globSrcJson, optBaseSrc )
    .pipe(deleteListsFromJson( ));
}

/**
 * copies files captured by `globSrcStatic` to web directory `www` or `www_dev`
 * @returns promise
 */
function copySrcStatic ()
{
  // copies everything that isn't otherwise processed
  return src( globSrcStatic, optBaseSrc )
    .pipe(dest(destinationPath));
}

function compileHtmlFromSrcJson (callback, watchedChange=false)
{
  return src( globSrcJson, optBaseSrc )
    .pipe(compileDataToHtml(callback, watchedChange ))
    .pipe(dest(destinationPath));
}


function processAllHandlebars ( callback, watchedChange=false )
{
  // console.log('processAllHandlebars');
  handlebarsRegisterAllPartials();
  // this processes every Handlebars page in content
  return src( globSrcHandlebars, optBaseSrc )
    .pipe(compileHandlebarsfromStream(callback, watchedChange))
    .pipe(dest(destinationPath));
}

function processSrcText ( callback, watchedChange=false )
{
  return src( globSrcText, optBaseSrc )
    .pipe(processTextinStream( callback, watchedChange=false ));
}

function processSrcImage ( callback, watchedChange=false )
{
  return src( globSrcImage, optBaseSrc )
    .pipe(processImageinStream( callback, watchedChange=false ));
}

function minifyJS ()
{
  return src( globSrcJS,  optBaseSrc)
    .pipe(minify(
    {
      ext: { min: '.js' },
      noSource: true,
      ignoreFiles: ['*-ignore.js']
    }
    ))
    .pipe(dest(destinationPath));
}

// --------------------------------------------------------------------

// --------------------------------------------------------------------
// Tasks - Build

function processJSinStream ()
{
  // Create a stream through which each image passes
  var stream = through.obj(function (file=new Vinyl(), enc, callBack)
  {
    console.log(file.contents.toString());

    // make sure the file goes through the next gulp plugin
    this.push(file);

    // tell the stream engine that we are done with this file
    callBack();
  });

  // returning the file stream
  return stream;
}


// --------------------------------------------------------------------

// --------------------------------------------------------------------
// Tasks - Watching

function watchingSrcStatic ()
{
  var watchSrcStatic = watch(globSrcStatic);

  // --> WATCH the files that are simply copied to the destination <--
  watchSrcStatic.on('add', () => { copySrcStatic(); });
  watchSrcStatic.on('change', () => { copySrcStatic(); });
  watchSrcStatic.on('unlink', (thisPath) =>
  {
    const relPath = path.relative(srcContentBasePath, thisPath); // get relative path from srcContent
    const destFilePath = path.join( destinationPath, relPath); // get path to dest
    // remove the file
    FS.rmSync(destFilePath, { force:true });
  });

  return watchSrcStatic;
}

/**
 * Garbage Collection for the watchers because we create files via script in a compile or to help during development
 * We want to remove all generated artifacts if the original is deleted
 * @param {*} deletedFilePath
 */
function garbageCollection ( deletedFilePath )
{
  // get the type of the file and the handle garbage collection accordingly
  const extension = path.extname(deletedFilePath);
  const dataPath = deletedFilePath.replace(extension,'.json');
  if (extension=='.txt' && FS.existsSync(dataPath))
  {
    // - it exists, first grab that data
    const contentJson = JSON.parse(FS.readFileSync(dataPath, 'utf8'));
    // then delete the json file - this is garbage collection
    FS.rmSync(dataPath, { force:true });

    // we've got a template... so this does create html which we need to delete
    if (contentJson.config.template)
    {
      // this is complex because we change directory names to id for a status
      const id = contentJson.props.id;
      const relPath = path.relative(srcContentBasePath, deletedFilePath); // get relative path from srcContent
      let relPathDirs = relPath.replace('.txt','');
      if (contentJson.config.type==='status') { relPathDirs = relPathDirs.replace(path.basename(relPathDirs), id); } // replace the directory name with id
      const destFilePath = path.join( destinationPath, relPathDirs +'.html'); // get path to dest

      // remove the file
      FS.rmSync(destFilePath, { force:true });
    }
  }
  else if (extension=='.hbs' && FS.existsSync(dataPath))
  {
    // - it exists, first grab that data
    // let contentJson = JSON.parse(FS.readFileSync(dataPath, 'utf8')); // we don't need it
    // then delete the json file - this is garbage collection
    FS.rmSync(dataPath, { force:true });

    const relPath = path.relative(srcContentBasePath, deletedFilePath); // get relative path from srcContent
    const destFilePath = path.join( destinationPath, relPath); // get path to dest
    // remove the html file
    FS.rmSync(destFilePath.replace('.hbs','.html'), { force:true });
  }
}

function watchingSrcText ()
{
  var watchingSrcText = watch(globSrcText, { ignoreInitial: true });

  // --> WATCH the files that are simply copied to the destination <--
  // note: on unlink "stats" is undefined
  watchingSrcText.on('unlink', (thisPath) =>
  {
    // console.log('watchingSrcText unlink '+thisPath);

    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    // so first we check for the LAST unlinked file here
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection ( deletedFilePath ); }
    // and then we track the CURRENT unlinked file here
    TRACKER.lastUnlinkedPath = thisPath;
  });

  watchingSrcText.on('add', (thisPath) =>
  {
    // console.log('watchingSrcText add '+thisPath);
    let ext = path.extname(thisPath);
    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const renamedFilePath = TRACKER.lastUnlinkedPath;
    const leftoverJsonPath = renamedFilePath.replace(ext,'.json');
    TRACKER.lastUnlinkedPath = '';
    // console.log('lastUnlinkedPath '+renamedFilePath);

    // CHECK FOR FILE RENAME
    const stats = FS.statSync(thisPath);
    // on file creation (a true 'add') these values are identical
    // thus if they are different then this file was renamed
    if (stats.birthtimeMs!=stats.ctimeMs)
    {
      // YES it was a file rename
      // move the leftover json file
      if (renamedFilePath!=='' && FS.existsSync(leftoverJsonPath))
      {
        // need to determine if this .json needs to be copied
        const contentJson = JSON.parse(FS.readFileSync(leftoverJsonPath, 'utf-8'));
        // there's a contentPath ref which is broken
        if ( contentJson.config.contentPath && !FS.existsSync(contentJson.config.contentPath) )
        {
          // update the content reference
          contentJson.config.contentPath = path.relative(PROJECT_PATH,thisPath);
          // remake the JSON file in the changed location/or with changed name
          FS.writeFileSync(thisPath.replace(ext,'.json'), JSON.stringify(contentJson, null, 2) );
        }

        // remove the old json file
        FS.rmSync(leftoverJsonPath, { force:true });
      }
      // nothing else to do here
      return;
    }
    // END FILE RENAME
    // BEGIN FILE ADD
    // if we deleted something previously, we might have to do something about it
    else if (renamedFilePath!=='') { garbageCollection ( renamedFilePath ); }

    // do not automatically create a .json file
    // because .txt might just be .txt and not some content to be compiled with a template into a page.
    // there's stuff to do on file add before we get to this
    // is this "content"?

    // json already exist?

    // is there text in the file?
    let fileContents = FS.readFileSync(thisPath, 'utf-8');
    if (fileContents!='')
    {
      processSrcText(()=>{}, true);
    }
  });

  watchingSrcText.on('change', (thisPath) =>
  {
    // console.log('watchingSrcText change '+thisPath);

    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    TRACKER.lastUnlinkedPath = '';
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection ( deletedFilePath ); }

    // proceed with the change
    processSrcText(()=>{}, true);
  });

  return watchingSrcText;
}

function watchingSrcHandlebars ()
{
  // --> WATCH the templates and manage their data files <--
  var watchGlobHandlebars = watch(globSrcHandlebars, { ignoreInitial: false });

  watchGlobHandlebars.on('unlink', (thisPath) =>
  {
    // console.log('watchingSrcHandlebars unlink ');

    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    // so first we check for the LAST unlinked file here
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection ( deletedFilePath ); }
    // and then we track the CURRENT unlinked file here
    TRACKER.lastUnlinkedPath = thisPath;
  });
  watchGlobHandlebars.on('add', (thisPath) =>
  {
    // console.log('watchingSrcHandlebars add ');

    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const renamedFilePath = TRACKER.lastUnlinkedPath;
    const leftoverJsonPath = renamedFilePath.replace('.hbs','.json');
    TRACKER.lastUnlinkedPath = '';

    // CHECK FOR FILE RENAME
    const stats = FS.statSync(thisPath);
    // on file creation (a true 'add') these values are identical
    // thus if they are different then this file was renamed
    if (stats.birthtimeMs!=stats.ctimeMs)
    {
      // YES it was a file rename
      // move the leftover json file
      if (renamedFilePath!=='' && FS.existsSync(leftoverJsonPath))
      {
        const contentJson = JSON.parse(FS.readFileSync(leftoverJsonPath, 'utf-8'));
        // remake the JSON file in the changed location/or with changed name
        FS.writeFileSync(thisPath.replace('.hbs','.json'), JSON.stringify(contentJson, null, 2) );

        // remove the old json file
        FS.rmSync(leftoverJsonPath, { force:true });
      }
      // nothing else to do here
      return;
    }
    // END FILE RENAME
    // BEGIN FILE ADD
    // if we deleted something previously, we might have to do something about it
    else if (renamedFilePath!=='') { garbageCollection ( renamedFilePath ); }

    // create a JSON file for the data that this template will use
    createDummyJsonDataForHandlebars(thisPath, leftoverJsonPath);

    processAllHandlebars(()=>{}, true);
  });
  watchGlobHandlebars.on('change', () =>
  {
    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    TRACKER.lastUnlinkedPath = '';
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection ( deletedFilePath ); }

    processAllHandlebars(()=>{}, true);
  });


  watchGlobHandlebars.restart = () =>
  {
    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    TRACKER.lastUnlinkedPath = '';
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection ( deletedFilePath ); }

    watchGlobHandlebars.close();
    watchGlobHandlebars = watchingSrcHandlebars ();
  };

  return watchGlobHandlebars;
}

function watchingTemplates (watcherSrc)
{
  var watchingTemplates = watch(globTemplates);
  watchingTemplates.on('change', () =>
  {
    // console.log('watchingTemplates change');
    // everytime there is a change in /src/templates,
    //   process all of the partial templates
    handlebarsRegisterAllPartials();
    //   and restart the src handlebars watcher
    watcherSrc.restart();
  });

  return watchingTemplates;
}

function watchingAllHandlebars ()
{
  let watcherSrc = watchingSrcHandlebars ();

  watchingTemplates (watcherSrc);
}

function watchingSrcJson ()
{
  // --> WATCH the data files <--
  var watchGlobJson = watch(globSrcJson, { ignoreInitial: false });
  // DATA FILE DELETED OR RENAMED
  watchGlobJson.on('unlink', (thisPath) =>
  {
    log('unlink', 'watchingSrcJson', thisPath, 'info');

    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    // so first we check for the LAST unlinked file here
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection(deletedFilePath); }
    // and then we track the CURRENT unlinked file here
    TRACKER.lastUnlinkedPath = thisPath;
  });
  // DATA FILE ADDED
  watchGlobJson.on('add', (thisPath) =>
  {
    log('add', 'watchingSrcJson', thisPath, 'info');

    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const renamedFilePath = TRACKER.lastUnlinkedPath;
    TRACKER.lastUnlinkedPath = '';

    // CHECK FOR FILE RENAME
    const stats = FS.statSync(thisPath);
    // on file creation (a true 'add') these values are identical
    // thus if they are different then this file was renamed
    if (stats.birthtimeMs!=stats.ctimeMs)
    {
      // YES it was a file rename

      // nothing else to do here
      return;
    }
    // END FILE RENAME
    // BEGIN FILE ADD
    // since it was not a rename there is a chance that there was a delete in the past
    // if we deleted something previously, we might have to do something about it
    else  if (renamedFilePath!=='') { garbageCollection(renamedFilePath); }

    // not really anything to do here
  });
  // DATA CHANGE
  watchGlobJson.on('change', (thisPath) =>
  {

    log('change', 'watchingSrcJson', thisPath, 'info');
    // this code needed because an 'unlink' event is ambiguous
    // 'unlink' can be either a file delete or rename/move
    const deletedFilePath = TRACKER.lastUnlinkedPath;
    TRACKER.lastUnlinkedPath = '';
    // if we deleted something previously, we might have to do something about it
    if (deletedFilePath!=='') { garbageCollection(deletedFilePath); }

    // and this is what we really want - compile when the data changes
    compileHtmlFromSrcJson(()=>{}, true);
  });
}

// --------------------------------------------------------------------

const initialize = series(wipeLists, processSrcImage, minifyJS, copySrcStatic, processSrcText, compileHtmlFromSrcJson);
exports.initialize = initialize;

const watching = parallel(watchingSrcStatic, watchingSrcText, watchingAllHandlebars, watchingSrcJson);
exports.watching = watching;

const develop = series(initialize);
exports.develop = develop;

const build = series(initialize);
exports.build = build;

exports.default = series(develop, watching);
