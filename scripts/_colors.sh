#!/bin/bash


# FG colors
c_1='\033[0;1m'  # bright/bold
c_2='\033[0;2m'  # dim/faint
c_4='\033[0;4m'  # underscore
c_7='\033[0;7m'  # reverse
c_r='\033[0;31m' # red
c_g='\033[0;32m' # green
c_y='\033[0;33m' # yellow
c_b='\033[0;36m' # cyan
# Clear the color
c__='\033[0m'