#!/bin/bash

# color include
source ./scripts/_colors.sh

# arguments
while getopts "p:b:f:" flag
do
    case "${flag}" in
        p) path=${OPTARG};;
        b) build=${OPTARG};;
        f) force=${OPTARG};;
    esac
done
if [ -z "$path" ]
then
  path="."
fi
if [ -z "$build" ]
then
  build="www"
fi

# destroy and recreate target
rm -rf "$path/$build"
mkdir -p "$path/$build/css"

# sass 
#   - compiles css if there are any more recent changes than the target css file
if [ "$build" = "www" ]
then
  sass --update --style=compressed --no-source-map "$path/src/styles/style.scss":"$path/www/css/style.css"
else
  sass --update "$path/src/styles/style.scss":"$path/www_dev/css/style.css"
fi
# build content

if [ -z "$force" ]
then
  npx gulp build --buildTarget="$build" --projectPath="$path"
else
  npx gulp build --buildTarget="$build" --projectPath="$path" --forceContentProcessing
fi


# images
# if command -v exiftool &> /dev/null
# then
#   echo -e "${c_g}exiftool - adding license metadata to www/**/*.jpg${c__}"
#   # get the license info from the package.json
#   license=`cat package.json | grep -o '"license": "[^"]*'  | grep -o '[^"]*$'`
#   exiftool -license="$license" www/**/*.jpg -overwrite_original &> /dev/null
# fi
