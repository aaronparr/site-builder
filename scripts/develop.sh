#!/bin/bash
trap "exit" INT TERM ERR
trap "kill 0" EXIT

source ./scripts/_colors.sh

# arguments
while getopts p: flag
do
    case "${flag}" in
        p) path=${OPTARG};;
    esac
done
if [ -z "$path" ]
then
  path="."
fi

# delete www_dev then recreate it
rm -rf "$path/www_dev"
mkdir -p "$path/www_dev/css"

# sass watcher
#   - compiles css if there are any more recent changes than the target css file
#   - then watches for changes and recompiles css on the fly as needed
sass --update "$path/src/styles/style.scss":"$path/www_dev/css/style.css"
sass -w "$path/src/styles/style.scss":"$path/www_dev/css/style.css" &

# content watcher
#   - initializes content
#   - then watches for changes
npx gulp initialize --buildTarget=www_dev --projectPath="$path"
npx gulp watching --buildTarget=www_dev --projectPath="$path" &

# webserver - spun up just for this
if [ -f ~/.ssh/localhost.pem ]
then
  echo -e "${c_g}Serving from ${c_b}$path/www_dev${c_g} via ${c_b}HTTPS${c__}${c_g} ...${c__}"
  npx serve --ssl-cert ~/.ssh/localhost.pem --ssl-key ~/.ssh/localhost.key "$path/www_dev" &
else
  echo -e "${c_g}Serving from ${c_b}$path/www_dev${c_g} via  ${c_b}HTTP${c__}${c_g} ...${c__}"
  npx serve "$path/www_dev" &
fi

# sleep for a second. serve gets a chance to put the address in the clipboard
sleep 1
# google chrome - launched for conveniencex
if [[ "$OSTYPE" == "darwin"* ]]; then
  open $( npx clipboard ) &
elif [[ "$OSTYPE" == "cygwin"* ]] || [[ "$OSTYPE" == "msys"* ]]; then
  start chrome --new-window $( npx clipboard ) &
fi

wait