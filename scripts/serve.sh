#!/bin/bash
trap "exit" INT TERM ERR
trap "kill 0" EXIT

source ./scripts/_colors.sh

# arguments
while getopts "p:s:" flag
do
    case "${flag}" in
        p) path=${OPTARG};;
        s) served=${OPTARG};;
    esac
done
if [ -z "$path" ]
then
  path="."
fi
if [ -z "$served" ]
then
  served="www_dev"
fi

# webserver - spun up just for this
if [ -f ~/.ssh/localhost.pem ]
then
  echo -e "${c_g}Serving from ${c_b}$path/$served${c_g} via ${c_b}HTTPS${c__}${c_g} ...${c__}"
  npx serve --ssl-cert ~/.ssh/localhost.pem --ssl-key ~/.ssh/localhost.key "$path/$served" &
else
  echo -e "${c_g}Serving from ${c_b}$path/$served${c_g} via  ${c_b}HTTP${c__}${c_g} ...${c__}"
  npx serve "$path/$served" &
fi

# sleep for a second. serve gets a chance to put the address in the clipboard
sleep 1
# google chrome - launched for conveniencex
if [[ "$OSTYPE" == "darwin"* ]]; then
  open $( npx clipboard ) &
elif [[ "$OSTYPE" == "cygwin"* ]] || [[ "$OSTYPE" == "msys"* ]]; then
  start chrome --new-window $( npx clipboard ) &
fi

wait