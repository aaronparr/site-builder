#!/bin/bash

# color include
source ./scripts/_colors.sh

# install comand line tools when npm install is called
echo -e "${c_g}Checking command line tools...${c__}"

# sass cli
echo -e "${c_1}sass${c__}          https://sass-lang.com/"
if ! command -v sass &> /dev/null
then
  echo -e "${c_2}missing${c__}"
  if command -v brew &> /dev/null
  then
    echo -e "${c_1}> brew install sass/sass/sass${c__}"
    brew install sass/sass/sass
  elif command -v choco &> /dev/null
  then
    echo -e "${c_1}> choco install sass${c__}"
    choco install sass
  else
    echo -e "${c_1}> npm install -g sass${c__}"
    npm install -g sass
  fi
else
  echo -e "${c_2}checked${c__}"
fi

echo -e "${c_g}...finished checking command line tools.${c__}"