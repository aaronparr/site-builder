# SITE BUILDER

Site Builder is a website generator used from the command line.

## INSTALLATION
`npm install` will install everything you need.

### RECOMMENDATIONS
1. I highly recommend that you install [sass](https://sass-lang.com/) manually before executing `npm install`. You can get the best versions for your use from the sass website. The node version is slower.
2. Add index.js to your path with the alias `site` or similar.
    * `$ chmod +x index.js` to make it executable
    * `$ ln -s ~/Code/site-builder/index.js ~/Code/.bin/site` To make a symlink for the command. **NOTE:** `~/Code/.bin/` is on my path and I keep my code in `~/Code`.

## STATUS
Current status of this project is that it is fully functional but unpolished. Also the watcher in development mode is very limited and needs a rewrite. So until I do that I recommend that if you don't see your changes in development mode, stop the develop process, then launch it again to force the changes through.


## HELP TEXT
```
USAGE:
ARG                  MEANING
[-c|create]          Create a new Site Builder project for a website.
    <PATH>             <PATH> to a directory for the new project. "newWebsite" will be created in the current directory if <PATH> is not provided.
    [-t|templates]     Add starter documentation and templates to a project for you to learn from.
[-b|build]           Build the website.
    <PATH>             <PATH> to the project to build. The current directory is default.
    [www|dev]          www for a production build (default), dev for development.
    [-f|force]         Force a rebuild of all content.
[-d|develop]         Execute watcher scripts to autobuild and serve the website during local development.
    <PATH>             <PATH> to the project you are working on. The current directory is default.
[-s|serve]           Serve a Site Builder project locally.
    <PATH>             <PATH> to the Site Builder directory to serve. The current directory is default.
    [www|dev]          www for production, dev for development (default).
[-h|help]            Display this message.
```